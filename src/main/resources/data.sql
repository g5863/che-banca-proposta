DELETE FROM TUTORIALS;
DELETE FROM PROPOSTA_STRUMENTO;
DELETE FROM PORTAFOGLIO_STRUMENTO;
DELETE FROM STRUMENTO;
DELETE FROM TIPOSTRUMENTO;
DELETE FROM PROPOSTA;
DELETE FROM PORTAFOGLIO;
DELETE FROM UTENTS;

INSERT INTO TUTORIALS(id, title, description, published) VALUES (1, 'USA', 'tutorial_americano', false);
INSERT INTO TUTORIALS(id, title, description, published) VALUES (2, 'France', 'tutorial_francese', false);
INSERT INTO TUTORIALS(id, title, description, published) VALUES (3, 'Brazil', 'tutorial_braziliano', true);
INSERT INTO TUTORIALS(id, title, description, published) VALUES (4, 'Italy', 'tutorial_italiano', true);
INSERT INTO TUTORIALS(id, title, description, published) VALUES (5, 'Canada', 'tutorial_canadese', false);

INSERT INTO UTENTS(id, nome, cognome, email, username, password, abi, code) VALUES (1, 'Pierpaolo', 'Nonnis', 'nonnis87@tiscali.it', 'pnonnis', '123456789', '05034', '12T45');

INSERT INTO TIPOSTRUMENTO(id, tipo, descrizione) VALUES(1, 'Obbligazione', 'Titolo di credito emesso per raccogliere denaro a debito');
INSERT INTO TIPOSTRUMENTO(id, tipo, descrizione) VALUES(2, 'Azione',  'Titolo finanziario rappresentativo di una quota della proprieta di una societa per azioni');
INSERT INTO TIPOSTRUMENTO(id, tipo, descrizione) VALUES(3, 'Marginazione', 'Negoziazione che consente all''nvestitore di poter acquistare o vendere investendo soltanto una parte della liquidita necessaria');
INSERT INTO TIPOSTRUMENTO(id, tipo, descrizione) VALUES(4, 'Fondo Azionario', 'Organismo che raccoglie risorse dal pubblico dei risparmiatori, attraverso l''emissione di quote, e le investe come un unico patrimonio');
INSERT INTO TIPOSTRUMENTO(id, tipo, descrizione) VALUES(5, 'Fondo Obbligazionario', 'Fondo comune composto interamente da Titoli di Stato e da Bond');
INSERT INTO TIPOSTRUMENTO(id, tipo, descrizione) VALUES(6, 'Future', 'Contratto a termine standardizzato con il quale le parti si impegnano a scambiare una certa attivita (finanziaria o reale) a un prezzo prefissato e con liquidazione differita a una data futura');

INSERT INTO STRUMENTO(id, isin, nome, descrizione, valuta, data_inizio, data_fine, tipostrumento_id, livello_rischio, tasso_interesse, prezzo_carico, prezzo_mercato) VALUES (1, 'DE000A1ZZ010','Bmw Us Cap Tf', 'Obbligazione Bmw Us Cap Tf 0,625% Ap22 Eur', 'EUR', '2021-07-08T16:30', '2024-07-07T00:00', 1, 2, 0.625, 100.4, 102.00);
INSERT INTO STRUMENTO(id, isin, nome, descrizione, valuta, data_inizio, data_fine, tipostrumento_id, livello_rischio, tasso_interesse, prezzo_carico, prezzo_mercato) VALUES (2, 'IE0004488262','Mediolanum Ch Financial Eq Evolt L A', 'Fondo Mediolanum Ch Financial Eq Evolt L A', 'EUR', '2022-03-22T16:30', '2024-07-07T00:00', 4, 3, 0.13, 6000.00 , 6000.00 );
INSERT INTO STRUMENTO(id, isin, nome, descrizione, valuta, data_inizio, data_fine, tipostrumento_id, livello_rischio, tasso_interesse, prezzo_carico, prezzo_mercato) VALUES (3, 'IT0005439861','A.B.P. NOCIVELLI', 'Azione EURONEXT GROWTH MILAN A.B.P. NOCIVELLI', 'EUR', '2022-02-24T16:30', '2022-05-24T00:00', 2, 3, 0.57, 3.55, 3.55);
INSERT INTO STRUMENTO(id, isin, nome, descrizione, valuta, data_inizio, data_fine, tipostrumento_id, livello_rischio, tasso_interesse, prezzo_carico, prezzo_mercato) VALUES (4, 'IT0019041414','FTSEMIB Jun 22', 'FTSEMIB Jun 22', 'EUR', '2022-06-01T00:30', '2022-11-30T23:59', 6, 1, 0.10, 24430, 27120);

INSERT INTO PORTAFOGLIO(id, liquidita, utente_id) VALUES (1, 10000.00, 1);
INSERT INTO PORTAFOGLIO(id, liquidita, utente_id) VALUES (2, 157000.00, 1);

INSERT INTO PORTAFOGLIO_STRUMENTO(id, quantita, strumento_id, portafoglio_id, create_date_time, update_date_time) VALUES(1, 15, 3, 1, sysdate, sysdate);
INSERT INTO PORTAFOGLIO_STRUMENTO(id, quantita, strumento_id, portafoglio_id, create_date_time, update_date_time) VALUES(2, 1, 2, 1, sysdate, sysdate);
INSERT INTO PORTAFOGLIO_STRUMENTO(id, quantita, strumento_id, portafoglio_id, create_date_time, update_date_time) VALUES(3, 2, 4, 1, sysdate, sysdate);

INSERT INTO PORTAFOGLIO_STRUMENTO(id, quantita, strumento_id, portafoglio_id, create_date_time, update_date_time) VALUES(4, 1, 1, 2, sysdate, sysdate);
INSERT INTO PORTAFOGLIO_STRUMENTO(id, quantita, strumento_id, portafoglio_id, create_date_time, update_date_time) VALUES(5, 5, 4, 2, sysdate, sysdate);

INSERT INTO PROPOSTA(id, code,descrizione, stato, portafoglio_id) VALUES (1, 'DE8789', 'Proposta per grandi vedute', 'WAIT' , 1);
INSERT INTO PROPOSTA(id, code,descrizione, stato, portafoglio_id) VALUES (2, 'AZ5001', 'Cessione totale', 'WAIT' , 2);

INSERT INTO PROPOSTA_STRUMENTO(id,operazione, quantita, strumento_id, proposta_id, create_date_time, update_date_time) VALUES (1, 'Acquista', 5, 1, 1, sysdate, sysdate);
INSERT INTO PROPOSTA_STRUMENTO(id,operazione, quantita, strumento_id, proposta_id, create_date_time, update_date_time) VALUES (2, 'Vendi', 1, 2, 1, sysdate, sysdate);

INSERT INTO PROPOSTA_STRUMENTO(id,operazione, quantita, strumento_id, proposta_id, create_date_time, update_date_time) VALUES (3, 'Vendi', 1, 1, 2, sysdate, sysdate);
INSERT INTO PROPOSTA_STRUMENTO(id,operazione, quantita, strumento_id, proposta_id, create_date_time, update_date_time) VALUES (4, 'Vendi', 5, 4, 2, sysdate, sysdate);
