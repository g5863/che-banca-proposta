package com.chebanca.proposta.dto;

import java.io.Serializable;
import java.util.List;

import com.chebanca.proposta.models.Portafoglio;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UtenteDTO implements Serializable {
	

	private static final long serialVersionUID = 4628928393175627122L;

	private long id;	
	private String nome;
	private String cognome;
	private String email;
	private String username;
	private String password;
	private String code;
	private String abi;
	private String expiration;
	private List<Portafoglio> portafogli;

}
