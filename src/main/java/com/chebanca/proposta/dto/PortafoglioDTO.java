package com.chebanca.proposta.dto;

import java.io.Serializable;
import java.util.List;

import com.chebanca.proposta.models.PortafoglioStrumento;
import com.chebanca.proposta.models.Proposta;
import com.chebanca.proposta.models.Utente;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PortafoglioDTO implements Serializable{

	private static final long serialVersionUID = 1852221921842042130L;

	private long id;
	private Utente utente;
	private Double liquidita;
	
	@JsonIgnoreProperties("portafoglio")
	private List<PortafoglioStrumento> portafoglioStrumento;
	
	@JsonIgnoreProperties("portafoglio")
	private List<Proposta> proposta;

}