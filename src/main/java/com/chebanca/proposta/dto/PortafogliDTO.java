package com.chebanca.proposta.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PortafogliDTO {

	private List<PortafoglioDTO> portafogli;
}
