package com.chebanca.proposta.controllers;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chebanca.proposta.models.Tutorial;
import com.chebanca.proposta.services.interfaces.TutorialInterface;
import com.chebanca.proposta.utils.PathConstants;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class TutorialController {

	@Autowired
	private TutorialInterface tutorialInterface;

	@GetMapping(PathConstants.TUTORIALS)
	public ResponseEntity<List<Tutorial>> getAllTutorials(@RequestParam(required = false) String title) {
		List<Tutorial> tutorials = tutorialInterface.searchAllTutorials(title);
		if (Objects.isNull(tutorials))
			return new ResponseEntity<>(tutorials, HttpStatus.INTERNAL_SERVER_ERROR);
		else if (tutorials.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		return new ResponseEntity<>(tutorials, HttpStatus.OK);

	}

	@GetMapping(PathConstants.TUTORIAL_BY_ID)
	public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id) {
		Optional<Tutorial> tutorialData = tutorialInterface.searchById(id);
		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(PathConstants.TUTORIALS)
	public ResponseEntity<Tutorial> createTutorial(@RequestBody Tutorial tutorial) {
		Tutorial tutorialResponse = tutorialInterface.createTutorial(tutorial);
		if (Objects.isNull(tutorialResponse))
			return new ResponseEntity<>(tutorialResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(tutorialResponse, HttpStatus.CREATED);
	}

	@PutMapping(PathConstants.TUTORIAL_BY_ID)
	public ResponseEntity<Tutorial> updateTutorial(@PathVariable("id") long id, @RequestBody Tutorial tutorial) {
		Tutorial tutorialData = tutorialInterface.updateTutorial(id, tutorial);
		if (tutorialData != null) {
			return new ResponseEntity<>(tutorialData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping(PathConstants.TUTORIAL_BY_ID)
	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
		return new ResponseEntity<>(tutorialInterface.deleteTutorial(id));
	}

	@DeleteMapping(PathConstants.TUTORIALS)
	public ResponseEntity<HttpStatus> deleteAllTutorials() {
		return new ResponseEntity<>(tutorialInterface.deleteAllTutorials());
	}

	@GetMapping(PathConstants.PUBLISHED)
	public ResponseEntity<List<Tutorial>> findByPublished() {

		List<Tutorial> tutorials = tutorialInterface.findByPublished();
		if (Objects.isNull(tutorials))
			return new ResponseEntity<>(tutorials, HttpStatus.INTERNAL_SERVER_ERROR);
		else if (tutorials.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		return new ResponseEntity<>(tutorials, HttpStatus.OK);
	}
}
