package com.chebanca.proposta.controllers;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chebanca.proposta.models.Proposta;
import com.chebanca.proposta.services.interfaces.PropostaInterface;
import com.chebanca.proposta.utils.PathConstants;

@RestController
@RequestMapping(value="/api/proposta")
public class PropostaController {

	@Autowired
	private PropostaInterface propostaInterface;

	
	@GetMapping(PathConstants.ALL_PROPOSES)
	public ResponseEntity<List<Proposta>> getPortafoglio(){
		List<Proposta> proposte = propostaInterface.searchAllProposes();
		if (Objects.isNull(proposte))
			return new ResponseEntity<>(proposte, HttpStatus.INTERNAL_SERVER_ERROR);
		else if (proposte.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		return new ResponseEntity<>(proposte, HttpStatus.OK);

	}

	//metodo che restituisce il portafoglio quindi al suo interno sono presenti sia le proposte che gli strumenti attivi 
		//metodo che restituisce le proposte dell utente in stato WAIT,ACCEPTED, REFUSED escludendo quelle piu vecchie di 15 giorni
			//eclude le proposte in stato WAIT_PAY questo stato verrà modificato in seguito alla callback
		
		//metodo che restituisce gli strumenti associati all utente
	
	//metodo che effettua una transazione di acuisto\vendita oppure non effettua alcuna transazione ma modifica lo stato della proposta in not accepted.
		
		//acquista chiama un servizio esterno per effettuare il pagamento
		
		//vendita chiama un servizio esterno che permette di vendere il titolo e incrementare la propria liquidità sul portafoglio
	
		//callback
			//un chiamante esterno invoca il nostro servizio esposto passando un token(composto da nomeutente.codiceproposta) ed eventuali dati che affermano che il pagamento è stato concluso con successo,
			//questi dati ricevuti non contengono valori sensibili quali numero carte di credito...
			//verranno memorizzati sul db in una tabella dedicata alle operazioni di pagamento 
			//Azioni possibili
				//invio mail utente in caso di pagamento con successo o fallito
				//invio mail a agente finanziario
				//modifica valore liquidità sul portafoglio del cliente
	
	//
//	@GetMapping(PathConstants.TUTORIALS)
//	public ResponseEntity<List<Tutorial>> getAllProposesByUser() {
//		List<Tutorial> tutorials = tutorialInterface.searchAllTutorials(title);
//		if (Objects.isNull(tutorials))
//			return new ResponseEntity<>(tutorials, HttpStatus.INTERNAL_SERVER_ERROR);
//		else if (tutorials.isEmpty())
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//
//		return new ResponseEntity<>(tutorials, HttpStatus.OK);
//
//	}
//
//	@GetMapping(PathConstants.TUTORIAL_BY_ID)
//	public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id) {
//		Optional<Tutorial> tutorialData = tutorialInterface.searchById(id);
//		if (tutorialData.isPresent()) {
//			return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//	}
//
//	@PostMapping(PathConstants.TUTORIALS)
//	public ResponseEntity<Tutorial> createTutorial(@RequestBody Tutorial tutorial) {
//		Tutorial tutorialResponse = tutorialInterface.createTutorial(tutorial);
//		if (Objects.isNull(tutorialResponse))
//			return new ResponseEntity<>(tutorialResponse, HttpStatus.INTERNAL_SERVER_ERROR);
//		return new ResponseEntity<>(tutorialResponse, HttpStatus.CREATED);
//	}
//
//	@PutMapping(PathConstants.TUTORIAL_BY_ID)
//	public ResponseEntity<Tutorial> updateTutorial(@PathVariable("id") long id, @RequestBody Tutorial tutorial) {
//		Tutorial tutorialData = tutorialInterface.updateTutorial(id, tutorial);
//		if (tutorialData != null) {
//			return new ResponseEntity<>(tutorialData, HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//	}
//
//	@DeleteMapping(PathConstants.TUTORIAL_BY_ID)
//	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
//		return new ResponseEntity<>(tutorialInterface.deleteTutorial(id));
//	}
//
//	@DeleteMapping(PathConstants.TUTORIALS)
//	public ResponseEntity<HttpStatus> deleteAllTutorials() {
//		return new ResponseEntity<>(tutorialInterface.deleteAllTutorials());
//	}
//
//	@GetMapping(PathConstants.PUBLISHED)
//	public ResponseEntity<List<Tutorial>> findByPublished() {
//
//		List<Tutorial> tutorials = tutorialInterface.findByPublished();
//		if (Objects.isNull(tutorials))
//			return new ResponseEntity<>(tutorials, HttpStatus.INTERNAL_SERVER_ERROR);
//		else if (tutorials.isEmpty())
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//
//		return new ResponseEntity<>(tutorials, HttpStatus.OK);
//	}
}
