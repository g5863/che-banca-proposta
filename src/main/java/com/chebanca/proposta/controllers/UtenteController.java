package com.chebanca.proposta.controllers;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chebanca.proposta.models.Utente;
import com.chebanca.proposta.services.interfaces.UtenteInterface;
import com.chebanca.proposta.utils.PathConstants;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class UtenteController {

	@Autowired
	private UtenteInterface utenteInterface;

	@PostMapping(PathConstants.USERS)
	public ResponseEntity<Utente> createTutorial(@RequestBody Utente utente) {
		Utente utenteResponse = utenteInterface.createUtente(utente);
	
		if (Objects.isNull(utenteResponse))
			return new ResponseEntity<>(utenteResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(utenteResponse, HttpStatus.CREATED);
	}
}
