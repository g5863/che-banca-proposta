package com.chebanca.proposta.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chebanca.proposta.models.Utente;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utils {
	

	private static final String DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	private static final String ANONYMOUS_USER = "anonymousUser";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	
	public static boolean listNullOrEmpty(Set<?> list) {
		 return !(list!=null && !list.isEmpty());
	}
	
	public static boolean listNullOrEmpty(List<?> list){
        return !(list!=null && !list.isEmpty());
    }
	
	public static boolean stringNullOrEmpty(String field) {
        return !(field != null && !field.trim().isEmpty());
    }
	
	public static boolean booleanIsNull(Boolean field) {
        return (field == null);
    }
	
	public static String convertBooleanFilterToString(Boolean request) {
		return (request == null) ? null : request ? "Y" : "N";
   } 
	
	
	public static String formatDateTimeToString(Date dateParam) {
		SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT);
		format.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		return format.format(dateParam);
	}
	
	public static String convertDateExcel(String dateParam) {
		 DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                 Locale.ITALY);
		 String returnDate = null;
         Date d;
         try
         {
             d = sdf.parse(dateParam);
             returnDate = (new SimpleDateFormat(DATETIME_FORMAT)).format(d);
         } catch (ParseException e) {
 			LOGGER.error("Utils.formatDateTimeStringToDate error {} {}", e.getMessage(), e);
 			e.printStackTrace();
 		}
		return returnDate;
	}
	
	public static Date formatDateTimeStringToDate(String dateTime){
		try {
			SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT);
			return format.parse(dateTime);
		} catch (ParseException e) {
			LOGGER.error("Utils.formatDateTimeStringToDate error {} {}", e.getMessage(), e);
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static String objectToXML(Object obj) {
        try{
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(obj, sw);
            return sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
            LOGGER.error("Utils.objectToXML error {} {}", e.getMessage(), e);
        }
		return null;
    }
	
	
	public static String objectToJson(Object object) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper(); 
		return mapper.writeValueAsString(object);
	}
	
	public static Object jsonToObject(String json, Object object) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper(); 
		return mapper.readValue(json, object.getClass());
	}
	


}