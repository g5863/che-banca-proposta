package com.chebanca.proposta.utils;

import java.io.Serializable;

public class Constants implements Serializable{

	private static final long serialVersionUID = 4369124264456304123L;

	public static final String HEADER= "authorization";
	public static final String SECRET_KEY= "Springbootjwttutorial";
	public static final String BEARER_TOKEN= "Bearer ";
	public static final String ISSUER= "ducat-springboot-jwttoken";
	public static final String CODE = "code";
	public static final String ABI = "abi";
	public static final String SUB = "sub";
	public static final String ISS = "iss";
	public static final String IAT = "iat";
}
