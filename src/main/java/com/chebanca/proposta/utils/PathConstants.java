package com.chebanca.proposta.utils;

public class PathConstants {
	private static final String STANDARD_MESSAGE_UTILITY_CLASS = "Utility Class";
	
	private PathConstants() {
		throw new IllegalAccessError(STANDARD_MESSAGE_UTILITY_CLASS);
	}
	
	public static final String TUTORIALS = "/tutorials";
	public static final String USERS = "/utenti";
	public static final String TUTORIAL_BY_ID = "/tutorials/{id}";
	public static final String PUBLISHED = "/tutorials/published";

	public static final String PORTFOLIO = "/portfolio";
	public static final String ALL_PROPOSES = "/all";
	public static final String DETAIL_STRUMENT = "/details/{id_strumento}";
	public static final String ACCEPT_REFUSED = "/accept";

}
