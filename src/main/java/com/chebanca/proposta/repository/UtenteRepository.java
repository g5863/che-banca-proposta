package com.chebanca.proposta.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.chebanca.proposta.models.Utente;

public interface UtenteRepository extends CrudRepository<Utente, Long>  {
	public List<Utente> findByCode(String code);
}
