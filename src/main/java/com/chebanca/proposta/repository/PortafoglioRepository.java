package com.chebanca.proposta.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.chebanca.proposta.models.Portafoglio;
import com.chebanca.proposta.models.Utente;

public interface PortafoglioRepository extends CrudRepository<Portafoglio, Long>  {
	public Set<Portafoglio> findByUtente(Utente utente);
}
