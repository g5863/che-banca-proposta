package com.chebanca.proposta.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name="PROPOSTA_STRUMENTO")
public class PropostaStrumento implements Serializable {

	private static final long serialVersionUID = 4540447657617698788L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="S_PROPOSTA_STRUMENTO")
	@SequenceGenerator(name = "S_PROPOSTA_STRUMENTO", sequenceName = "S_PROPOSTA_STRUMENTO")
	private Long id;
	
	@ManyToOne(optional=false)
	@JsonIgnoreProperties("propostaStrumento")
	private Proposta proposta;
	
	@ManyToOne(optional=false)
	private Strumento strumento;
	
	@Column(nullable=false)
	private Integer quantita;
	
	@Column(nullable=false)
	private String operazione;//compra/vendi
	
	@CreationTimestamp
	@Column(nullable=false, updatable=false)
	private Timestamp createDateTime;
	
	@UpdateTimestamp
	private Timestamp updateDateTime;
}
