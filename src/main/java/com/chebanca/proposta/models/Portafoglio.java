package com.chebanca.proposta.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Portafoglio implements Serializable{

	private static final long serialVersionUID = 1852221921842042130L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne(optional=false)
	@JsonIgnoreProperties("portafogli")
	private Utente utente;
	
	@Column(name = "liquidita")
	private Double liquidita;
	
	@OneToMany(mappedBy="portafoglio")
	@JsonIgnoreProperties("portafoglio")
	private List<PortafoglioStrumento> portafoglioStrumento;
	
	@OneToMany(mappedBy="portafoglio")
	@JsonIgnoreProperties("portafoglio")
	private List<Proposta> proposta;

}