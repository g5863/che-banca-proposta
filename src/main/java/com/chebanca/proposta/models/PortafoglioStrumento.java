package com.chebanca.proposta.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name="PORTAFOGLIO_STRUMENTO")
public class PortafoglioStrumento implements Serializable{
	//contiene SOLO gli strumenti ACQUISTATI in portafoglio
	private static final long serialVersionUID = 5601532691566512598L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="S_PORTAFOGLIO_STRUMENTO")
	@SequenceGenerator(name = "S_PORTAFOGLIO_STRUMENTO", sequenceName = "S_PORTAFOGLIO_STRUMENTO")
	private Long id;
	
	@ManyToOne(optional=false)
	@JsonIgnoreProperties("portafoglioStrumento")
	private Portafoglio portafoglio;
	
	@ManyToOne(optional=false)
	private Strumento strumento;
	
	@Column(nullable=false)
	private Integer quantita;
	
	@CreationTimestamp
	@Column(nullable=false, updatable=false)
	private Timestamp createDateTime;
	
	@UpdateTimestamp
	private Timestamp updateDateTime;
}
