package com.chebanca.proposta.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name = "utents")
public class Utente implements Serializable{

	private static final long serialVersionUID = -1310429901522088121L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="S_UTENTE")
	@SequenceGenerator(name = "S_UTENTE", sequenceName = "S_UTENTE")
	private long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "cognome")
	private String cognome;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	private String password;
	
	@Column(name= "code")
	private String code;//codice assegnato dal CRM
	
	@Column(name = "abi")
	private String abi;//Associazione Bancaria Italiana
	
	@Column(name= "expiration")
	private String expiration;//timeout Token
	
	@OneToMany(mappedBy="utente")
	@JsonIgnoreProperties("utente")
	private List<Portafoglio> portafogli;
	
}
