package com.chebanca.proposta.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
public class Proposta implements Serializable{

	private static final long serialVersionUID = -7189862933853349131L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "stato")
	private String stato; //stato proposta WAIT ACCEPT REFUSED (andranno considerate quelle rifiutato da non piu di 15 giorni mettere logica nella query)
	
	@Column(name = "descrizione")
	private String descrizione;
	
	@OneToMany(mappedBy="proposta")
	@JsonIgnoreProperties("proposta")
	private List<PropostaStrumento> propostaStrumento;

	@ManyToOne(optional=false)
	@JsonIgnoreProperties("proposta")
	private Portafoglio portafoglio;
}