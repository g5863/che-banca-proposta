package com.chebanca.proposta.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.ToString;

@Entity
@Getter
@ToString
@Table(name = "tipostrumento")
public class TipoStrumento implements Serializable {

	private static final long serialVersionUID = -7905141404220339194L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "descrizione")
	private String descrizione;
	
	@OneToMany(mappedBy="tipostrumento")
//	@JsonIgnoreProperties("tipostrumento")
	private List<Strumento> strumenti;
	
}