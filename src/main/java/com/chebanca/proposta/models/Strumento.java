package com.chebanca.proposta.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Strumento implements Serializable {

	private static final long serialVersionUID = 5392522738636507478L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "isin")
	private String isin;

	@Column(name = "nome")
	private String nome;

	@Column(name = "descrizione")
	private String descrizione;

	@Column(name = "valuta")
	private String valuta;

	@Column(name = "data_inizio")
	private String dataInizio;

	@Column(name = "data_fine")
	private String dataFine;

	@ManyToOne(optional=false)
	@JsonIgnoreProperties("strumenti")
	private TipoStrumento tipostrumento;

	@Column(name = "livello_rischio")
	private Integer livelloRischio;

	@Column(name = "tasso_interesse")
	private Double tassoInteresse;

	@Column(name = "prezzo_carico")
	private Double prezzoCarico;

	@Column(name = "prezzo_mercato")
	private Double prezzoMercato;
}