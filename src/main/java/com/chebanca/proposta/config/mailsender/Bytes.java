package com.chebanca.proposta.config.mailsender;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.GeneratedMessageV3;

/**
 * Transform your data structure into byte[].
 */
public class Bytes {

	private static final Logger LOGGER = LoggerFactory.getLogger(Bytes.class);
	
	private Bytes() {}

	public static byte[] getBytes(Collection<?> list) {
		LOGGER.debug("");
		StringBuilder sb = new StringBuilder();
		for (Object object : list) {
			sb.append(object.toString()).append("\n");
		}
		return sb.toString().getBytes();
	}

	public static byte[] getBytes(List<?> list) {
		LOGGER.debug("");
		StringBuilder sb = new StringBuilder();
		for (Object object : list) {
			sb.append(object.toString()).append("\n");
		}
		return sb.toString().getBytes();
	}

	public static byte[] getBytes(Set<?> set) {
		StringBuilder sb = new StringBuilder();
		for (Object object : set) {
			sb.append(object.toString()).append("\n");
		}
		return sb.toString().getBytes();
	}

	public static byte[] getBytes(Throwable t) {
		if (t == null)
			return "Throwable not found".getBytes();
		StringWriter writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		t.printStackTrace(printWriter);
		printWriter.flush();
		return writer.toString().getBytes();
	}

	public static byte[] getBytes(Double d) {
		return String.valueOf(d).getBytes();
	}

	public static byte[] getBytes(Integer i) {
		return String.valueOf(i).getBytes();
	}

	public static byte[] getBytes(BigDecimal bd) {
		return bd.toString().getBytes();
	}

	public static byte[] getBytes(Map<?, ?> map) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<?, ?> entry : map.entrySet()) {
			sb.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
		}
		return sb.toString().getBytes();
	}

	public static byte[] getBytes(StringBuilder stringBuilder) {
		return stringBuilder.toString().getBytes();
	}

	public static byte[] getBytes(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		return getBytes(is);
	}

	public static byte[] getBytes(String filepath) throws IOException {
		return getBytes(new File(filepath));
	}

	public static byte[] getBytes(InputStream inputStream) throws IOException {
		InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		StringBuilder sb = new StringBuilder();
		try {
			String line = br.readLine();
			while (line != null) {
				sb.append(line).append("\n");
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		return getBytes(sb);
	}
	
	public static String toString(byte[] bytes) {
		return new String(bytes);
	}
	
	public static byte[] getBytes(GeneratedMessageV3 message) {
		return message.toByteArray();
	}

}
