package com.chebanca.proposta.config.mailsender;

import java.beans.XMLDecoder;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * It is used to configure the SMTP server. <br>
 * It extends {@link MailSenderConfiguration}.
 * <p>
 * The configuration can be built in different ways: <br>
 * <ul>
 * <li>{@link #loadDefaultConfig(String...)} allows to load the default
 * configuration that specifies the SMTP server and the sender's email; it is
 * necessary to specify the receiveirs;</li>
 * <li>{@link #loadConfig(InputStream)} allows to load an xml in
 * {@link InputStream} format;</li>
 * <li>{@link #loadConfig(String)} allows to load an xml in {@link String}
 * format;</li>
 * <li>{@link #loadConfig(File)} allows to load an xml in {@link File}
 * format.</li>
 * </ul>
 * <br>
 * 
 * The xml to be loaded must have the following form: <br>
 * {@code
 * }<br>
 * {@code<?xml version="1.0" encoding="UTF-8"?>
 * }<br>
 * {@code<java version="1.6.0_20" class="java.beans.XMLDecoder"> 
 * }<br>
 * {@code<object
 * }<br>
 * {@code	class="com.gatelab.resource.mailsender.MailSenderConfiguration">
 * }<br>
 * {@code	<void property="host">
 * }<br>
 * {@code		<string>your-host-smtp</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="port">
 * }<br>
 * {@code		<string>your-port</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="username">
 * }<br>
 * {@code		<string>your-username</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="password">
 * }<br>
 * {@code		<string>your-password</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="sender">
 * }<br>
 * {@code		<string>sender@mail.com</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="receiver">
 * }<br>
 * {@code		<string>receiver1@mail.com,receiver2@mail.com</string>
 * }<br>
 * {@code</void>
 * }<br>
 * {@code</object>
 * }<br>
 * {@code</java>
 * }
 */
public class MailConfig extends MailSenderConfiguration implements Serializable {
	private static final long serialVersionUID = -7740556673133128353L;

	private static final String MAIL_REGEXP = "^[A-Za-z0-9+_.-]+@(.+)$";

	/**
	 * It returns the builder to use in case of manual configuration
	 * {@link MailSenderConfiguration}.
	 * 
	 * @return
	 */
	public static MailConfigBuilder getBuilder() {
		return new MailConfigBuilder();
	}

	private MailConfig() {
		super();
	}

	private MailConfig(MailSenderConfiguration mailSenderConfiguration) {
		super.setHost(mailSenderConfiguration.getHost());
		super.setPort(mailSenderConfiguration.getPort());
		super.setUsername(mailSenderConfiguration.getUsername());
		super.setPassword(mailSenderConfiguration.getPassword());
		super.setSender(mailSenderConfiguration.getSender());
		super.setReceiver(mailSenderConfiguration.getReceiver());
		super.setReceiverCC(mailSenderConfiguration.getReceiverCC());
		super.setReceiverBCC(mailSenderConfiguration.getReceiverBCC());
		super.setIsAuthenticated(mailSenderConfiguration.getIsAuthenticated());
		super.setUseTLS(mailSenderConfiguration.getUseTLS());
	}
	
	/**
	 * Load default {@link MailConfig} configuration. <br>
	 * Only stmp server is set in the default. You must specify the receivers using
	 * a comma-separated list of email addresses. <br>
	 * <br>
	 * <b>Load configuration specifying only one receiver</b><br>
	 * {@code MailConfig.loadDefaultConfig(mail1@mail.it)}<br>
	 * <br>
	 * <b>Load configuration specifying more than one receivers</b><br>
	 * {@code MailConfig.loadDefaultConfig(mail1@mail.it,mail2@mail.it)}
	 * 
	 * @param receivers
	 * @return
	 * @throws Exception
	 */
	public static MailConfig loadDefaultConfig(List<String> receivers) throws Exception {
		if (receivers == null || receivers.size() == 0) {
			throw new IllegalArgumentException("Missing receivers");
		}

		Pattern pattern = Pattern.compile(MAIL_REGEXP);
		StringBuilder receiversBuilder = new StringBuilder();
		int countEmptyMail = 0;
		Set<String> mailNotRecognized = new HashSet<String>();

		for (String receiver : receivers) {

			if (receiver.isEmpty()) {
				countEmptyMail++;
				continue;
			}

			Matcher matcher = pattern.matcher(receiver);
			if (matcher.matches()) {
				if (!receiversBuilder.toString().isEmpty())
					receiversBuilder.append(",");
				receiversBuilder.append(receiver);
			} else {
				if (!receiver.isEmpty())
					mailNotRecognized.add(receiver);
				continue;
			}
		}

		StringBuilder finalOutput = new StringBuilder();

		if (countEmptyMail > 0) {
			finalOutput.append("There are empty");
		}

		if (!mailNotRecognized.isEmpty()) {
			if (!finalOutput.toString().isEmpty()) {
				finalOutput.append(" and not recognized mail (").append(mailNotRecognized).append(")");
			} else {
				finalOutput.append("There are not recognized mail (").append(mailNotRecognized).append(")");
			}
		}

		if (!finalOutput.toString().isEmpty()) {
			throw new IllegalArgumentException(finalOutput.toString());
		}

		MailConfigBuilder mcb = MailConfig.getBuilder();
		mcb.host("10.10.10.52");
		mcb.port("25");
//		mcb.username("43mail12533");
//		mcb.password("Smtp12533");
		mcb.sender("pierpaolo.nonnis@ags-it.com");
		mcb.isAuthenticated("false");
		mcb.useTLS("false");
		mcb.receiver(receiversBuilder.toString());
		MailConfig mc = mcb.build();
		MailConfig mailConfig = new MailConfig(mc);

		return mailConfig;
	}

	/**
	 * Load {@link MailConfig} xml or proeprties mail configuration in {@link InputStream} format.
	 * <p>
	 * See {@link MailConfig} for more details.
	 * 
	 * @param mailConfigStream
	 * @return
	 * @throws IOException 
	 */
	public static MailConfig loadConfig(InputStream mailConfigStream) throws IOException {
		return loadConfig(Bytes.getBytes(mailConfigStream));
	}

	/**
	 * Load {@link MailConfig} mail configuration from a {@link Properties}.
	 * <p>
	 * Parameters must be the following:<br>
	 * <ul>
	 * <li><b>host</b>=<i>your-server-stmp</i></li>
	 * <li><b>port</b>=<i>your-port</i></li>
	 * <li><b>username</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>password</b>=<i>your-password</i></li>
	 * <li><b>sender</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>receiver</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>isAuthenticated</b>=<i>true|false</i></li>
	 * <li><b>useTLS</b>=<i>true|false</i></li>
	 * </ul>
	 * <p>
	 * See {@link MailConfig} for more details.
	 * 
	 * @param properties
	 * @return
	 */
	public static MailConfig loadConfig(Properties properties) {
		MailConfigBuilder mb = MailConfig.getBuilder();
		mb.host(properties.getProperty("host"));
		mb.port(properties.getProperty("port"));
		mb.username(properties.getProperty("username"));
		mb.password(properties.getProperty("password"));
		mb.sender(properties.getProperty("sender"));
		mb.receiver(properties.getProperty("receiver"));
		mb.receiverCC(properties.getProperty("receiverCC"));
		mb.receiverBCC(properties.getProperty("receiverBCC"));
		mb.isAuthenticated(properties.getProperty("isAuthenticated"));
		mb.useTLS(properties.getProperty("useTLS"));
		return mb.build();
	}

	/**
	 * Load {@link MailConfig} xml or properties mail configuration in {@link File} format.
	 * <p>
	 * See {@link MailConfig} for more details.
	 * 
	 * @param mailConfigFile must be contain a xml or properties
	 * @return
	 * @throws IOException 
	 */
	public static MailConfig loadConfig(File mailConfigFile) throws IOException {
		return loadConfig(Bytes.getBytes(mailConfigFile));
	}

	/**
	 * Load {@link MailConfig} xml or properties mail configuration from a specified file path.
	 * <p>
	 * See {@link MailConfig} for more details.
	 * 
	 * @param mailConfigFilePath
	 * @return
	 * @throws IOException 
	 */
	public static MailConfig loadConfig(String mailConfigFilePath) throws IOException {
		return loadConfig(Bytes.getBytes(MailConfig.class.getClassLoader().getResourceAsStream(mailConfigFilePath)));
	}
	
	/**
	 * Load {@link MailConfig} xml or properties mail configuration from a byte[].
	 * <p>
	 * See {@link MailConfig} for more details.
	 * 
	 * @param mailConfigFilePath
	 * @return
	 * @throws IOException 
	 */
	public static MailConfig loadConfig(byte[] mailConfigBytes) throws IOException {
		String mailConfigContent = Bytes.toString(mailConfigBytes);
		if (MailFactory.isXml(mailConfigContent)) {
			XMLDecoder xdec = null;
			try {
				xdec = new XMLDecoder(new ByteArrayInputStream(mailConfigBytes));
				return new MailConfig((MailSenderConfiguration) xdec.readObject());
			} catch (Exception ex) {
				throw ex;
			} finally {
				if (xdec != null)
					xdec.close();
			}
		} else if (MailFactory.isProps(mailConfigContent)) {
			return new MailConfig(loadConfig(getPropsBy(mailConfigContent)));
		} else {
			throw new IllegalArgumentException("Unsupported mail config content bytes");
		}
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

	/**
	 * It is used to build a {@link MailConfig} configuration.
	 */
	public static class MailConfigBuilder implements Serializable {

		private static final long serialVersionUID = -941452147423831186L;

		private String _host;
		private String _port;
		private String _username;
		private String _password;
		private String _sender;
		private String _receiver;
		private String _receiverCC;
		private String _receiverBCC;
		private String _isAuthenticated;
		private String _useTLS;

		private MailConfigBuilder() {
		}

		public MailConfigBuilder host(String host) {
			this._host = host;
			return this;
		}

		public MailConfigBuilder port(String port) {
			this._port = port;
			return this;
		}

		public MailConfigBuilder username(String username) {
			this._username = username;
			return this;
		}

		public MailConfigBuilder password(String password) {
			this._password = password;
			return this;
		}

		public MailConfigBuilder sender(String sender) {
			this._sender = sender;
			return this;
		}

		public MailConfigBuilder receiver(String receiver) {
			this._receiver = receiver;
			return this;
		}

		public MailConfigBuilder receiverCC(String receiverCC) {
			this._receiverCC = receiverCC;
			return this;
		}

		public MailConfigBuilder receiverBCC(String receiverBCC) {
			this._receiverBCC = receiverBCC;
			return this;
		}

		public MailConfigBuilder isAuthenticated(String isAuthenticated) {
			this._isAuthenticated = isAuthenticated;
			return this;
		}

		public MailConfigBuilder useTLS(String useTLS) {
			this._useTLS = useTLS;
			return this;
		}

		/**
		 * Build {@link MailConfig} configuration by performing a check of validity.
		 * 
		 * @return
		 * @throws IllegalArgumentException
		 */
		public MailConfig build() throws IllegalArgumentException {
			StringBuilder checkValidityBuilder = new StringBuilder();

			checkValidityBuilder.append("Null or empty arguments: ");

			if (_host == null || _host.isEmpty()) {
				checkValidityBuilder.append("host");
			}

			if (_port == null || _port.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("port");
			}

			if (_username == null || _username.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("username");
			}

			if (_password == null || _password.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("password");
			}

			if (_sender == null || _sender.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("sender");
			}

			if (_receiver == null || _receiver.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("receiver");
			}

			if (_receiverCC == null || _receiverCC.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("receiverCC");
			}

			if (_receiverBCC == null || _receiverBCC.isEmpty()) {
				if (checkValidityBuilder.toString().isEmpty()) {
					checkValidityBuilder.append(", ");
				}
				checkValidityBuilder.append("receiverBCC");
			}

			if (_isAuthenticated == null) {
				_isAuthenticated = "true";
			}

			if (_useTLS == null) {
				_useTLS = "false";
			}

			if (checkValidityBuilder.toString().isEmpty()) {
				throw new IllegalArgumentException(checkValidityBuilder.toString());
			}

			return new MailConfig(new MailSenderConfiguration(_host, _port, _username, _password, _sender, _receiver,
					_receiverCC, _receiverBCC, _isAuthenticated, _useTLS));
		}

	}	
	
	/**
	 * Check if a string is a java.util.Properties.
	 * 
	 * @param input
	 * @return
	 * @throws IOException
	 */
	private static Properties getPropsBy(String mailConfigFileContent) throws IOException {
		Properties props = new Properties();
		props.load(new StringReader(mailConfigFileContent));
		return props;
	}

}
