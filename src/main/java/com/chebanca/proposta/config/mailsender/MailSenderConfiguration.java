package com.chebanca.proposta.config.mailsender;

import java.io.Serializable;

/**
 * Represent an xml with server SMTP, senders and receivers configurations.
 * <p>
 * Example of xml: <br>
 * {@code
 * }<br>
 * {@code<?xml version="1.0" encoding="UTF-8"?>
 * }<br>
 * {@code<java version="1.6.0_20" class="java.beans.XMLDecoder"> 
 * }<br>
 * {@code<object
 * }<br>
 * {@code	class="com.gatelab.resource.mailsender.MailSenderConfiguration">
 * }<br>
 * {@code	<void property="host">
 * }<br>
 * {@code		<string>your-host-smtp</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="port">
 * }<br>
 * {@code		<string>your-port</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="username">
 * }<br>
 * {@code		<string>your-username</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="password">
 * }<br>
 * {@code		<string>your-password</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="sender">
 * }<br>
 * {@code		<string>sender@mail.com</string>
 * }<br>
 * {@code	</void>
 * }<br>
 * {@code	<void property="receiver">
 * }<br>
 * {@code		<string>receiver1@mail.com,receiver2@mail.com</string>
 * }<br>
 * {@code</void>
 * }<br>
 * {@code</object>
 * }<br>
 * {@code</java>
 * }
 */
public class MailSenderConfiguration implements Serializable {

	private static final long serialVersionUID = 7763748302634459610L;

	private String host;
	private String port;
	private String username;
	private String password;
	private String sender;
	private String receiver;
	private String receiverCC;
	private String receiverBCC;
	private String isAuthenticated;
	private String useTLS;

	public MailSenderConfiguration(String host, String port, String username, String password, String sender,
			String receiver, String receiverCC, String receiverBCC, String isAuthenticated, String useTLS) {
		super();
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.sender = sender;
		this.receiver = receiver;
		this.receiverCC = receiverCC;
		this.receiverBCC = receiverBCC;
		this.isAuthenticated = isAuthenticated;
		this.useTLS = useTLS;
	}

	public MailSenderConfiguration() {
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverCC() {
		return receiverCC;
	}

	public void setReceiverCC(String receiverCC) {
		this.receiverCC = receiverCC;
	}

	public String getReceiverBCC() {
		return receiverBCC;
	}

	public void setReceiverBCC(String receiverBCC) {
		this.receiverBCC = receiverBCC;
	}
	
	public boolean checkValidity() {
		StringBuilder errorBuider = new StringBuilder();
		
		if (host == null || host.isEmpty()) {
			errorBuider.append("host");
		}
		
		if (port == null || port.isEmpty()) {
			if (!errorBuider.toString().isEmpty())
				errorBuider.append(", ");
			errorBuider.append("port");
		}
		
		if (username == null || username.isEmpty()) {
			if (!errorBuider.toString().isEmpty())
				errorBuider.append(", ");
			errorBuider.append("username");
		}
		
		if (password == null || password.isEmpty()) {
			if (!errorBuider.toString().isEmpty())
				errorBuider.append(", ");
			errorBuider.append("password");
		}
		
		if (sender == null || sender.isEmpty()) {
			if (!errorBuider.toString().isEmpty())
				errorBuider.append(", ");
			errorBuider.append("sender");
		}
		
		if (receiver == null || receiver.isEmpty()) {
			if (!errorBuider.toString().isEmpty())
				errorBuider.append(", ");
			errorBuider.append("receiver");
		}
		
		if (!errorBuider.toString().isEmpty()) {
			String msgErr = new StringBuilder("Missing mail configuration parameters: ").append(errorBuider.toString()).toString();
			throw new IllegalStateException(msgErr);
		}
		
		return true;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getIsAuthenticated() {
		return isAuthenticated != null ? isAuthenticated : "true";
	}

	public void setIsAuthenticated(String isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	public String getUseTLS() {
		return useTLS != null ? useTLS : "false";
	}

	public void setUseTLS(String useTLS) {
		this.useTLS = useTLS;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailSenderConfiguration [host=");
		builder.append(host);
		builder.append(", port=");
		builder.append(port);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", sender=");
		builder.append(sender);
		builder.append(", receiver=");
		builder.append(receiver);
		builder.append(", receiverCC=");
		builder.append(receiverCC);
		builder.append(", receiverBCC=");
		builder.append(receiverBCC);
		builder.append(", isAuthenticated=");
		builder.append(isAuthenticated);
		builder.append(", useTLS=");
		builder.append(useTLS);
		builder.append("]");
		return builder.toString();
	}

}
