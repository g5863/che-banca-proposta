package com.chebanca.proposta.config.mailsender;

/**
 * Represents the details of an error/warning.
 */
public class ExecutionDetail {

	private String message;
	private Throwable throwable;
	private long timestamp;

	public ExecutionDetail(String message, Throwable throwable, long timestamp) {
		super();
		this.message = message;
		this.throwable = throwable;
	}

	public ExecutionDetail(String message) {
		super();
		this.message = message;
	}

	public ExecutionDetail(Throwable throwable) {
		super();
		this.throwable = throwable;
	}

	public String getMessage() {
		return message;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExecutionDetail [message=");
		builder.append(message);
		builder.append(", throwable=");
		builder.append(throwable);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append("]");
		return builder.toString();
	}

}
