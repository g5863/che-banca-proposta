package com.chebanca.proposta.config.mailsender;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chebanca.proposta.config.mailsender.Attachments.Attachment;
import com.chebanca.proposta.config.mailsender.MailConfig.MailConfigBuilder;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import net.lingala.zip4j.exception.ZipException;

/**
 * Factory to build the email to send. There are two mail types:
 * <ul>
 * <li>quick mail (standard, html)</li>
 * <li>advanced mail</li>
 * </ul>
 * <p>
 * <hr>
 * <h1>Quick mail</h1><br>
 * Allows you to quickly create and send an email specifying only the subject
 * and the message. You can specify two body types: standard or html.<br>
 * <br>
 * <b>Standard</b> example: <br>
 * {@code MailFactory.createQuickMail ("name1.surname1@domain.com", "name2.surname2@domain.com"). quickSend ("Your subject", "Your message");}
 * <br>
 * <br>
 * <b>Html</b> example: <br>
 * {@code MailFactory.htmlMessage (true) .createQuickMail ("name1.surname1@domain.com", "name2.surname2@domain.com"). quickSend ("Your subject", "<html> <body> Your message </ body> </ html> ");}
 * <p>
 * <hr>
 * <h1>Advanced mail</h1> <br>
 * To manually build your email you can use the example given in bottom. <br>
 * An advanced email can have the following features: <br>
 * <ul>
 * <li>allows you to attache info, config, errors, warnings and
 * attachments;</li>
 * <li>allows you to send info, errors and warnings in the body of the
 * mail;</li>
 * <li>allows you to send a message in html format;</li>
 * <li>allows you to specify the task that generated the mail sending and its
 * relative outcome;</li>
 * <li>allows you to specify a custom message that will overwrite the default
 * one; in this case it is possible to access the task and task parameters as
 * with the <b>$<b> notation</b>.</li>
 * </ul>
 * <p>
 * {@code MailBuilder mb = MailFactory.createMail (MAIL_CONFIGURATION_XML));}
 * <br>
 *
 * {@code // Task} <br>
 * {@code mb.task ("My task");} <br>
 *
 * {@code // Configuration} <br>
 * {@code mb.addConfig ("key1", "value1"). addConfig ("key2", "value2");} <br>
 *
 * {@code // Info} <br>
 * {@code mb.addInfo ("info1"). addInfo ("info2");} <br>
 *
 * {@code // Warning} <br>
 * {@code mb.addWarning ("warning1", new Throwable ("warning1")). addWarning ("warning2", new Throwable ("warning2"));}
 * <br>
 *
 * {@code // Error} <br>
 * {@code mb.addError ("error1", new Throwable ("error1")). addError ("error2", new Throwable ("error2"));}
 * <br>
 *
 * {@code // Load an attachment} <br>
 * {@code mb.addAttachments (Attachments.getInstance ("path / to / attachment"));}
 * <br>
 *
 * {@code // Load an attachment and call it myAttachment} <br>
 * {@code mb.addAttachments (Attachments.getInstance ("myAttachment", "path / to / attachment"));}
 * <br>
 *
 * {@code // Load my byte [] into a file called my-attachment.txt} <br>
 * {@code mb.addAttachments (Attachments.getInstance ("my-attachment.txt", "My content" .getBytes ()));}
 * <br>
 *
 * {@code // Create a zip with two attachments} <br>
 * {@code mb.addAttachments ("zipName1.zip", Attachments.getInstance ("path / to / attachment1"), Attachments.getInstance ("path / to / attachment2"));}
 * <br>
 *
 * {@code // Create a zip with password from a file} <br>
 * {@code mb.addAttachments ("zipName2.zip", "mypassword", Attachments.getInstance ("path / to / attachment"));}
 * <br>
 *
 * {@code // Properties} <br>
 * {@code mb.printConfig (true) .printInfo (true); // print configurations and info}
 * <br>
 * {@code mb.printWarning (true) .printError (true); // print the causes of errors and warnings}
 * <br>
 * {@code mb.printWarningComplete (). printErrorComplete (); // print error and warning with stacktrace}
 * <br>
 * {@code mb.addErrorAsAttachment ("error1.txt"). addWarningAsAttachment ("warning1.txt");}
 * <br>
 *
 * {@code mb.subject ("Advanced mail configuration"). taskOutcomeFailure ();}
 * <br>
 *
 * {@code mb.send ();} <br>
 * } <br>
 * <p>
 * <hr>
 * <h1>Note</h1> Each method of this class contains a detail on the operation
 * and an example code that can be used during development. <br>
 * You can use MailSender.getMailConfigurations to print your mail
 * configurations. <br>
 */
public class MailFactory {

	private static Logger logger = LoggerFactory.getLogger(MailFactory.class);

	public static final String MAIL_CONFIGURATION_XML_FILE_NAME = "mail_configuration.xml";
	public static final String MAIL_CONFIGURATION_PROPS_FILE_NAME = "mail.properties";

	/**
	 * Task final states.
	 */
	private enum TaskOutcome {
		/**
		 * Task is finished successfully.
		 */
		SUCCESS,

		/**
		 * Task is finished successfully but there are some errors.
		 */
		SUCCESS_WITH_ERRORS,

		/**
		 * Task is failure.
		 */
		FAILURE
	}

	private MailFactory() {
	}

	/**
	 * Create quick mail using default Gate SMTP server configuration. <br>
	 * You must specify a comma separated list of receivers.<br>
	 * <br>
	 * Example: <br>
	 * {@code MailFactory.createMail("mail1@mail.com", "mail2@mail.com")}<br>
	 * <br>
	 * 
	 * See {@link MailFactory} for more details.<br>
	 * <br>
	 * 
	 * @param receivers
	 * @return
	 * @throws Exception
	 */
	public static MailBuilder createQuickMail(List<String> receivers) throws Exception {
		if (receivers == null || receivers.size() == 0) {
			throw new IllegalArgumentException("Missing receivers");
		}
		return new MailBuilder(MailConfig.loadDefaultConfig(receivers)).quickConfiguration(true);
	}

	/**
	 * Instantiates MailBuilder to configure mail using SMTP server default
	 * configuration. <br>
	 * If no configuration files have been specified it will generate an
	 * {@link IllegalStateException}.<br>
	 * 
	 * @return
	 */
	public static MailBuilder createMail() {
		try {
			return createMail(MAIL_CONFIGURATION_XML_FILE_NAME);
		} catch (Exception e) {
			logger.warn("Missing default xml mail configuration");
		}

		try {
			return createMail(MAIL_CONFIGURATION_PROPS_FILE_NAME);
		} catch (Exception e1) {
			logger.error("Missing default props mail configuration");
		}

		throw new IllegalStateException("Missing default mail configuration");
	}

	/**
	 * Instantiates MailBuilder to configure mail using GMAIL SMTP server
	 * configuration.
	 * <p>
	 * host: smtp.gmail.com <br>
	 * port: 587 <br>
	 * isAuthenticated: true <br>
	 * useTLS: true
	 * <p>
	 * 
	 * You must set username, password, sender and receivers.
	 * <p>
	 * 
	 * @return
	 */
	public static MailBuilder createMailUsingGmail() {
		MailConfigBuilder mb = MailConfig.getBuilder();
		mb.host("smtp.gmail.com");
		mb.port("587");
		mb.isAuthenticated("true");
		mb.useTLS("true");
		return new MailBuilder(mb.build());
	}

	/**
	 * Instantiates MailBuilder to configure mail using GMAIL SMTP server
	 * configuration.
	 * <p>
	 * host: smtp-mail.outlook.com <br>
	 * port: 587 <br>
	 * isAuthenticated: true <br>
	 * useTLS: true
	 * <p>
	 * 
	 * You must set username, password, sender and receivers.
	 * <p>
	 * 
	 * @return
	 */
	public static MailBuilder createMailUsingOutlook() {
		MailConfigBuilder mb = MailConfig.getBuilder();
		mb.host("smtp-mail.outlook.com");
		mb.port("587");
		mb.isAuthenticated("true");
		mb.useTLS("true");
		return new MailBuilder(mb.build());
	}

	/**
	 * Instantiates MailBuilder to configure mail using MailSenderConfiguration.<br>
	 * You can use {@link MailConfig} to configure
	 * {@link MailSenderConfiguration}.<br>
	 * <br>
	 * Example: <br>
	 * {@code MailFactory.createMail(MailConfig.loadConfig(MailConfig.class.getClassLoader().getResourceAsStream(MAIL_CONFIGURATION_XML)))}<br>
	 * <br>
	 * 
	 * See {@link MailFactory} for more details.<br>
	 * 
	 * @param mailConfig
	 * @return
	 */
	public static MailBuilder createMail(MailSenderConfiguration mailConfig) {
		if (mailConfig == null) {
			throw new NullPointerException("Missing mail sender configuration");
		}
		return new MailBuilder(mailConfig);
	}

	/**
	 * Instantiates MailBuilder to configure mail using InputStream.<br>
	 * <br>
	 * Example: <br>
	 * {@code MailFactory.createMail(inputStream); // inputStream is an xml or a properties with configuration to load}<br>
	 * <br>
	 * See {@link MailFactory} for more details.<br>
	 * 
	 * @param mailConfigInputStream
	 *            - Represent configuration to load in xml or {@link Properties}
	 *            format
	 * @return
	 * @throws IOException
	 */
	public static MailBuilder createMail(InputStream mailConfigInputStream) throws IOException {
		if (mailConfigInputStream == null) {
			throw new NullPointerException("Missing configuration input stream");
		}

		String inputString = new String(Bytes.getBytes(mailConfigInputStream));

		// E' un XML?
		if (isXml(inputString)) {
			return new MailBuilder(MailConfig.loadConfig(inputString.getBytes()));
		}

		// E' una Properties
		else if (isProps(inputString)) {
			Properties prop = new Properties();
			prop.load(new StringReader(inputString));
			return new MailBuilder(MailConfig.loadConfig(prop));
		}

		// Input non riconosciuto
		else {
			throw new IllegalArgumentException("Configuration input stream not recognized");
		}

	}

	/**
	 * Instantiates MailBuilder to configure mail using File.<br>
	 * <br>
	 * Example: <br>
	 * {@code MailFactory.createMail(yourXmlFile); // yourXmlFile è il tuo File (xml o properties)}<br>
	 * <br>
	 * 
	 * In case of {@link Properties} the parameters must be the following: :<br>
	 * <li><b>host</b>=<i>your-server-stmp</i></li>
	 * <li><b>port</b>=<i>your-port</i></li>
	 * <li><b>username</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>password</b>=<i>your-password</i></li>
	 * <li><b>sender</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>receiver</b>=<i>your.mail@domain.com</i></li><br>
	 * See {@link MailFactory} for more details.<br>
	 * <br>
	 * 
	 * @param mailConfigFile
	 *            - Represents file to load in xml or {@link Properties} format.
	 * @return
	 * @throws IOException
	 */
	public static MailBuilder createMail(File mailConfigFile) throws IOException {
		if (mailConfigFile == null) {
			throw new IllegalArgumentException("Missing xml mail configuration file");
		}

		String inputString = new String(Bytes.getBytes(mailConfigFile));

		// E' un XML?
		if (isXml(inputString)) {
			return new MailBuilder(MailConfig.loadConfig(mailConfigFile));
		}

		// E' una Properties
		else if (isProps(inputString)) {
			Properties prop = new Properties();
			prop.load(new StringReader(inputString));
			return new MailBuilder(MailConfig.loadConfig(prop));
		}

		// Input non riconosciuto
		else {
			throw new IllegalArgumentException("Configuration file not recognized");
		}
	}

	/**
	 * Instantiates MailBuilder to configure mail using xml or Properties file
	 * path.<br>
	 * <br>
	 * Example: <br>
	 * {@code MailFactory.createMail("your/file/path/mail_configuration.xml");}<br>
	 * <br>
	 * In case of {@link Properties} the parameters must be the following: :<br>
	 * <li><b>host</b>=<i>your-server-stmp</i></li>
	 * <li><b>port</b>=<i>your-port</i></li>
	 * <li><b>username</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>password</b>=<i>your-password</i></li>
	 * <li><b>sender</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>receiver</b>=<i>your.mail@domain.com</i></li><br>
	 * See {@link MailFactory} for more details.<br>
	 * <br>
	 * 
	 * @param mailConfigFilePath
	 *            - Represents xml or {@link Properties} file path.
	 * @return
	 * @throws IOException
	 */
	public static MailBuilder createMail(String mailConfigFilePath) throws IOException {
		if (mailConfigFilePath == null || mailConfigFilePath.isEmpty()) {
			throw new IllegalArgumentException("Missing mail configuration file path");
		}
		return new MailBuilder(MailConfig.loadConfig(mailConfigFilePath));
	}

	/**
	 * Instantiates MailBuilder to configure mail using Properties class.<br>
	 * <br>
	 * The parameters must be the following:<br>
	 * <li><b>host</b>=<i>your-server-stmp</i></li>
	 * <li><b>port</b>=<i>your-port</i></li>
	 * <li><b>username</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>password</b>=<i>your-password</i></li>
	 * <li><b>sender</b>=<i>your.mail@domain.com</i></li>
	 * <li><b>receiver</b>=<i>your.mail@domain.com</i></li><br>
	 * Example: <br>
	 * {@code MailFactory.createMail(yourProps"); // yourProps is your Properties object}<br>
	 * <br>
	 * See {@link MailFactory} for more details.<br>
	 * <br>
	 * 
	 * @param mailConfigProps
	 *            - Contains parameters to load.
	 * @return
	 * @throws FileNotFoundException
	 */
	public static MailBuilder createMail(Properties mailConfigProps) throws FileNotFoundException {
		if (mailConfigProps == null) {
			throw new IllegalArgumentException("Missing mail properties");
		}
		return new MailBuilder(MailConfig.loadConfig(mailConfigProps));
	}

	/**
	 * Builder for manual mail configuration.<br>
	 */
	public static class MailBuilder implements Serializable {
		private static final long serialVersionUID = -4775419089660556376L;

		private MailSenderConfiguration mailConfig;

		/**
		 * Are you setting up a quick mail?
		 */
		private boolean isQuickConfiguration = false;

		/**
		 * Task that generated the sending of the email.
		 */
		private String task;

		/**
		 * {@link #task} outcome.
		 */
		private TaskOutcome taskOutcome = TaskOutcome.SUCCESS;

		/**
		 * Is an html message?
		 */
		private Boolean isHtmlMessage = false;

		private Map<String, String> configs = new HashMap<String, String>();
		private Set<String> info = new HashSet<String>();
		private Set<ExecutionDetail> warnings = new HashSet<ExecutionDetail>();
		private Set<ExecutionDetail> errors = new HashSet<ExecutionDetail>();
		private List<Attachment> attachments = new ArrayList<Attachment>();

		private boolean printConfig = false;
		private boolean printInfo = false;
		private boolean printWarning = false;
		private boolean printError = false;
		private boolean printWarningComplete = false;
		private boolean printErrorComplete = false;

		private String subject;
		private boolean overrideMessage;

		private String message;

		private Map<String, Object> dataModel;

		/**
		 * Builds MailBuilder using MailSenderConfiguration.
		 * 
		 * @param mailConfig
		 */
		private MailBuilder(MailSenderConfiguration mailConfig) {
			this.mailConfig = mailConfig;
		}

		/**
		 * Builds MailBuilder using an xml file.
		 * 
		 * @param xmlPath
		 * @throws Exception
		 */
		private MailBuilder(String xmlPath) throws Exception {
			this.mailConfig = MailConfig.loadConfig(MailConfig.class.getClassLoader().getResourceAsStream(xmlPath));
		}

		/**
		 * Are you setting up a quick mail?
		 * 
		 * @return
		 */
		public boolean isQuickConfiguration() {
			return isQuickConfiguration;
		}

		/**
		 * Specify if mail is fast.
		 * 
		 * @param isQuickConfiguration
		 * @return
		 */
		private MailBuilder quickConfiguration(boolean isQuickConfiguration) {
			this.isQuickConfiguration = isQuickConfiguration;
			return this;
		}

		/**
		 * Returns mail subject.
		 * 
		 * @return
		 */
		public String getSubject() {
			return subject;
		}

		/**
		 * Set subject mail.
		 * 
		 * @param subject
		 * @return
		 */
		public MailBuilder subject(String subject) {
			this.subject = subject;
			return this;
		}

		/**
		 * Executes mail message build before getting message.
		 * 
		 * @return
		 */
		public String getMessage() {
			return message;
		}

		/**
		 * This method allows you to set the parameters you want to pass in your
		 * Freemarker template using a {@literal .properties} file
		 * 
		 * @param properties
		 *            A {@literal .properties} file containing the parameters you use in
		 *            your template
		 * @return
		 */
		public MailBuilder addParametersToTemplate(Properties properties) {
			Map<String, Object> map = new HashMap<>();
			@SuppressWarnings("unchecked")
			Enumeration<String> enums = (Enumeration<String>) properties.propertyNames();
			while (enums.hasMoreElements()) {
				String key = enums.nextElement();
				String value = properties.getProperty(key);
				map.put(key, value);
			}
			dataModel = map;
			return this;
		}

		/**
		 * This method allows you to set the parameters you want to pass in your
		 * Freemarker template using a Map object
		 * 
		 * @param parameters
		 *            The map containing the parameters you use in your template
		 * @return
		 */
		public MailBuilder addParametersToTemplate(Map<String, Object> parameters) {
			dataModel = parameters;
			return this;
		}

		/**
		 * Set custom mail body. <br>
		 * <b>If you want to use default message you can not invoke this method. Use
		 * this method only if you want to configure a custom message.</b>
		 * <p>
		 * 
		 * <b>DEFAULT</b> <br>
		 * You can use default without invoking {@link #message(String)}. <br>
		 * Example of default message: <br>
		 * <br>
		 * <i>Hi,<br>
		 * task "your-task" execution status: "your-outcome".</i><br>
		 * <br>
		 * <br>
		 * <b>CUSTOM</b><br>
		 * You can use custom message as follows:
		 * <ul>
		 * <li>custom message</li>
		 * <li>custom message with access to {@link #task} and {@link #taskOutcome}
		 * variables</li>
		 * <li>html message</li>
		 * </ul>
		 * <br>
		 * <b>Custom message</b><br>
		 * Write your message in string format.<br>
		 * <br>
		 * <b>Custom message with access to {@link #task} and {@link #taskOutcome}</b>
		 * <br>
		 * Write your message in string format accessing the variables as follows:<br>
		 * <li>to access the {@link #task} you can use <b>$task</b> string
		 * <li>to access the {@link #taskOutcome} you can use <b>$taskOutcome</b> string
		 * <br>
		 * Example: <br>
		 * <i>Hi, task <b>$task</b> is finished with <b>$taskOutcome</b></i><br>
		 * <br>
		 * <b>Html message</b><br>
		 * Enter mail body in html format. Invoke {@link #htmlMessage(boolean)} method
		 * to set html format. <br>
		 * If you want to use html template you can use
		 * {@link #templatedMessage(String)}. <br>
		 * In both cases (default and custom) the mail will be built keeping the info,
		 * config, warning and error structure unchanged. <br>
		 * <br>
		 * 
		 * @param message
		 */
		public MailBuilder message(String message) {
			this.overrideMessage = true;
			this.message = message;

			if (message.contains("$task")) {
				this.message = this.message.replace("$task", task);
			}

			if (message.contains("$taskOutcome")) {
				this.message = this.message.replace("$taskOutcome", taskOutcome.name());
			}

			return this;
		}

		/**
		 * Is an html message? Set true else set false.
		 * 
		 * @param isHtmlMessage
		 * @return
		 */
		public MailBuilder htmlMessage(boolean isHtmlMessage) {
			this.isHtmlMessage = isHtmlMessage;
			return this;
		}

		/**
		 * This method constructs a message using a <i>FreeMarker 2.3.26</i> template
		 * and creates html message. <br>
		 * You <b>must absolutely</b> use the {@link #addParametersToTemplate(Map)} or
		 * {@link #addParametersToTemplate(Properties)} method first to allow the
		 * template to catch the parameters, otherwise your email body will contain
		 * nothing.
		 * 
		 * @param templateFilePath
		 *            The absolute template file path to the FreeMarker template
		 * @return
		 * @throws IOException
		 *             When the template is not found in the path you are passing in
		 * @throws TemplateException
		 *             If the template was incorrectly written
		 */
		public MailBuilder htmlMessage(String templateFilePath) throws IOException, TemplateException {
			logger.info("Is html message? - {}", isHtmlMessage());
			Path path = Paths.get(templateFilePath);

			Configuration cfg = new Configuration(Configuration.VERSION_2_3_26);
			cfg.setDirectoryForTemplateLoading(path.getParent().toFile());
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

			Template tpl = cfg.getTemplate(path.getFileName().toString());
			Writer out = new StringWriter();
			if (dataModel != null) {
				tpl.process(dataModel, out);
			}
			this.message(out.toString());
			return this;
		}

		/**
		 * Is an html message?
		 * 
		 * @return
		 */
		public Boolean isHtmlMessage() {
			return isHtmlMessage;
		}

		/**
		 * Returns configurations map.<br>
		 * A configuration is a couple key/value, don't confuse with
		 * {@link MailSenderConfiguration}.
		 * 
		 * @return
		 */
		public Map<String, String> getConfigs() {
			return configs;
		}

		/**
		 * Adds a configuration. <br>
		 * You can insert any configuration in key/value format.<br>
		 * A configuration is a couple key/value, don't confuse with
		 * {@link MailSenderConfiguration}.
		 * 
		 * @param key
		 * @param value
		 * @return
		 */
		public MailBuilder addConfig(String key, String value) {
			if (this.configs == null) {
				this.configs = new HashMap<String, String>();
			}
			if (!this.configs.containsKey(key)) {
				this.configs.put(key, value);
			}
			return this;
		}

		/**
		 * Adds a configuration using a java.util.Properties file. <br>
		 * You can insert any configuration in key/value format.<br>
		 * A configuration is a couple key/value, don't confuse with
		 * {@link MailSenderConfiguration}.
		 * 
		 * @param key
		 * @param value
		 * @return
		 */
		public MailBuilder addConfig(Properties configProperties) {
			if (this.configs == null) {
				this.configs = new HashMap<String, String>();
			}
			for (String key : configProperties.stringPropertyNames()) {
				String value = configProperties.getProperty(key);
				configs.put(key, value);
			}
			return this;
		}

		/**
		 * Returns mail info.
		 * 
		 * @return
		 */
		public Set<String> getInfo() {
			return info;
		}

		/**
		 * Adds an info. <br>
		 * The info will be useful to receivers.
		 * 
		 * @param info
		 * @return
		 */
		public MailBuilder addInfo(String info) {
			if (this.info == null) {
				this.info = new HashSet<String>();
			}
			this.info.add(info);
			return this;
		}

		/**
		 * Returns errors caught during the execution of the process that generated the
		 * sending of the email.<br>
		 * See {@link #addError(String)}, {@link #addError(Throwable)} or
		 * {@link #addError(String, Throwable)} for more details.
		 * 
		 * @return
		 */
		public Set<ExecutionDetail> getErrors() {
			return errors;
		}

		/**
		 * Adds an error.<br>
		 * An error is an exception caught during the execution of your process. <br>
		 * Errors can be added at the end of the email body or sent as an attachment.
		 * <br>
		 * <br>
		 * See {@link #printError()}, {@link #printErrorComplete()},
		 * {@link #addErrorAsAttachment(String)}
		 * 
		 * @param message
		 *            - The error message
		 * @param error
		 *            - The exception
		 * @return
		 */
		public MailBuilder addError(String message, Throwable error) {
			if (this.errors == null) {
				this.errors = new HashSet<ExecutionDetail>();
			}
			this.errors.add(new ExecutionDetail(message, error, System.nanoTime()));
			return this;
		}

		/**
		 * Adds an error.<br>
		 * An error is an exception caught during the execution of your process. <br>
		 * Errors can be added at the end of the email body or sent as an attachment.
		 * <br>
		 * <br>
		 * <br>
		 * See {@link #printError()}, {@link #printErrorComplete()},
		 * {@link #addErrorAsAttachment(String)}
		 * 
		 * @param message
		 *            - The error message
		 * @return
		 */
		public MailBuilder addError(String message) {
			if (this.errors == null) {
				this.errors = new HashSet<ExecutionDetail>();
			}
			this.errors.add(new ExecutionDetail(message));
			return this;
		}

		/**
		 * Adds an error.<br>
		 * An error is an exception caught during the execution of your process. <br>
		 * Errors can be added at the end of the email body or sent as an attachment.
		 * <br>
		 * <br>
		 * <br>
		 * See {@link #printError()}, {@link #printErrorComplete()},
		 * {@link #addErrorAsAttachment(String)}
		 * 
		 * @param error
		 *            - The exception
		 * @return
		 */
		public MailBuilder addError(Throwable error) {
			if (this.errors == null) {
				this.errors = new HashSet<ExecutionDetail>();
			}
			this.errors.add(new ExecutionDetail(error));
			return this;
		}

		/**
		 * Returns warnings caught during the execution of the process that generated
		 * the sending of the email.<br>
		 * See {@link #addWarning(String)}, {@link #addWarning(Throwable)} or
		 * {@link #addWarning(String, Throwable)} for more details.
		 * 
		 * @return
		 */
		public Set<ExecutionDetail> getWarnings() {
			return warnings;
		}

		/**
		 * Adds a warning. <br>
		 * A warning is an alert caught during the execution of the process. <br>
		 * The warnings can be added at the end of the email body or sent as an
		 * attachment. <br>
		 * <br>
		 * See {@link #printWarning()}, {@link #printWarningComplete()},
		 * {@link #addWarningAsAttachment(String)}
		 * 
		 * @param message
		 * @param warning
		 * @return
		 */
		public MailBuilder addWarning(String message, Throwable warning) {
			if (this.warnings == null) {
				this.warnings = new HashSet<ExecutionDetail>();
			}
			this.warnings.add(new ExecutionDetail(message, warning, System.nanoTime()));
			return this;
		}

		/**
		 * Adds a warning. <br>
		 * A warning is an alert caught during the execution of the process. <br>
		 * The warnings can be added at the end of the email body or sent as an
		 * attachment. <br>
		 * <br>
		 * See {@link #printWarning()}, {@link #printWarningComplete()},
		 * {@link #addWarningAsAttachment(String)}
		 * 
		 * @param message
		 * @return
		 */
		public MailBuilder addWarning(String message) {
			if (this.warnings == null) {
				this.warnings = new HashSet<ExecutionDetail>();
			}
			this.warnings.add(new ExecutionDetail(message));
			return this;
		}

		/**
		 * Adds a warning. <br>
		 * A warning is an alert caught during the execution of the process. <br>
		 * The warnings can be added at the end of the email body or sent as an
		 * attachment. <br>
		 * <br>
		 * See {@link #printWarning()}, {@link #printWarningComplete()},
		 * {@link #addWarningAsAttachment(String)}
		 * 
		 * @param warning
		 * @return
		 */
		public MailBuilder addWarning(Throwable warning) {
			if (this.warnings == null) {
				this.warnings = new HashSet<ExecutionDetail>();
			}
			this.warnings.add(new ExecutionDetail(warning));
			return this;
		}

		/**
		 * Returns the attachments to send.<br>
		 * See {@link #addAttachments(Attachment...)},
		 * {@link #addAttachments(String, Attachment...)},
		 * {@link #addAttachments(String, String, Attachment...)},
		 * {@link #addErrorAsAttachment(String)}, {@link #addInfoAsAttachment(String)}
		 * or {@link #addWarningAsAttachment(String)} for more details.
		 * 
		 * @return
		 */
		public List<Attachment> getAttachments() {
			return attachments;
		}

		/**
		 * Load a list of attachments. <br>
		 * <br>
		 * You can use {@link Attachments} to build the attachments.<br>
		 * <br>
		 * 
		 * @param attachments
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addAttachments(Attachment... attachments)
				throws ZipException, IOException, URISyntaxException {
			if (attachments == null || attachments.length == 0) {
				throw new IllegalArgumentException("Missing attachments");
			}
			for (Attachment attachment : attachments) {
				this.attachments.add(attachment);
			}
			return this;
		}

		/**
		 * Creates a file zip with a list of attachments. <br>
		 * <br>
		 * You can add your file extension. Default is <b>.zip</b>.<br>
		 * <br>
		 * You can use {@link Attachments} to build your attachments.<br>
		 * <br>
		 * 
		 * @param finalZipAttachment
		 * @param attachments
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addAttachments(String zipFilename, Attachment... attachments)
				throws ZipException, IOException, URISyntaxException {
			if (attachments == null || attachments.length == 0) {
				throw new IllegalArgumentException("Missing attachments");
			}
			if (!zipFilename.contains(".")) {
				zipFilename = new StringBuilder(zipFilename).append(".zip").toString();
			}
			this.attachments.add(Attachments.getInstance(zipFilename, attachments));
			return this;
		}

		/**
		 * Creates a file zip with all file inside a folder. <br>
		 * <br>
		 * You can add your file extension. Default is <b>.zip</b>.<br>
		 * <br>
		 * 
		 * @param finalZipAttachment
		 * @param pathDir
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addAttachments(String zipFilename, String pathDir) throws ZipException, IOException {
			this.attachments.add(Attachments.getInstance(zipFilename, getAllFileInFolder(pathDir)));
			return this;
		}

		/**
		 * Creates a file zip with all file inside a folder. <br>
		 * <br>
		 * You can add your file extension. Default is <b>.zip</b>.<br>
		 * <br>
		 * 
		 * @param zipFilename
		 * @param zipPassword
		 * @param pathDir
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addAttachments(String zipFilename, String password, String pathDir)
				throws IOException, ZipException {
			this.attachments.add(Attachments.getInstance(zipFilename, password, getAllFileInFolder(pathDir)));
			return this;
		}

		private Attachment[] getAllFileInFolder(String pathDir) throws IOException {
			File folder = new File(pathDir);
			Attachment[] atts = new Attachment[0];
			for (final File fileEntry : folder.listFiles()) {
				if (!fileEntry.isDirectory()) {
					Attachment[] newArray = Arrays.copyOf(atts, atts.length + 1);
					newArray[atts.length] = Attachments.getInstance(fileEntry.getPath());
					atts = newArray;
				}
			}
			return atts;
		}

		/**
		 * Creates a cripted file zip with a list of attachments. <br>
		 * <br>
		 * You can add your file extension. Default is <b>.zip</b>.<br>
		 * <br>
		 * You can use {@link Attachments} to build your attachments.<br>
		 * <br>
		 * 
		 * @param zipFilename
		 * @param zipPassword
		 * @param attachments
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addAttachments(String zipFilename, String zipPassword, Attachment... attachments)
				throws ZipException, IOException, URISyntaxException {
			if (attachments == null || attachments.length == 0) {
				throw new IllegalArgumentException("Missing attachments");
			}
			if (!zipFilename.contains(".")) {
				zipFilename = new StringBuilder(zipFilename).append(".zip").toString();
			}
			this.attachments.add(Attachments.getInstance(zipFilename, zipPassword, attachments));
			return this;
		}

		/**
		 * Adds a new attachment containing a Protocol Buffer object coded into a stream
		 * of bytes <br>
		 * 
		 * @param filename
		 *            The name of the file you want to attach to your mail
		 * @param message
		 *            The generated protocol buffer message you want to send as an
		 *            attachment
		 * @return
		 * @throws InvalidProtocolBufferException
		 *             if the input is not a valid Protocol Buffer object
		 */
		public MailBuilder addAttachmentsProto(String filename, GeneratedMessageV3 message)
				throws InvalidProtocolBufferException {
			this.attachments.add(Attachments.getInstance(filename, Bytes.getBytes(message)));
			return this;
		}

		/**
		 * Adds a new attachment containing a Protocol Buffer object coded into a string
		 * which represents a JSON object. Finally this object is sent into a stream of
		 * bytes.
		 * 
		 * @param filename
		 *            The name of the file you want to attach to your mail
		 * @param proto
		 *            The Protocol Buffer object you want to send as an attachment
		 * @return
		 * @throws InvalidProtocolBufferException
		 *             if the input is not a valid Protocol Buffer object
		 */
		public MailBuilder addAttachmentsProtoToJson(String filename, GeneratedMessageV3 proto)
				throws InvalidProtocolBufferException {
			this.attachments.add(Attachments.getInstance(filename, JsonFormat.printer().print(proto).getBytes()));
			return this;
		}

		/**
		 * Creates an attachment with your info.<br>
		 * The attachment name is <b>filename</b>. The attachment is managed by the JVM
		 * and is not saved on the disk.
		 * 
		 * @param filename
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addInfoAsAttachment(String filename) throws ZipException, IOException, URISyntaxException {
			this.attachments.add(Attachments.getInstance(filename, Bytes.getBytes(info)));
			return this;
		}

		/**
		 * Creates an attachment with your warnings.<br>
		 * The attachment name is <b>filename</b>. The attachment is managed by the JVM
		 * and is not saved on the disk.
		 * 
		 * @param filename
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addWarningAsAttachment(String filename)
				throws ZipException, IOException, URISyntaxException {
			StringBuilder builder = new StringBuilder();
			for (ExecutionDetail executionDetail : warnings) {
				builder.append(executionDetail.getMessage()).append("\n")
						.append(Bytes.getBytes(executionDetail.getThrowable())).append("\n");
			}
			this.attachments.add(Attachments.getInstance(filename, builder.toString().getBytes()));
			return this;
		}

		/**
		 * Creates an attachment with your errors.<br>
		 * The attachment name is <b>filename</b>. The attachment is managed by the JVM
		 * and is not saved on the disk.
		 * 
		 * @param filename
		 * @return
		 * @throws ZipException
		 * @throws IOException
		 * @throws URISyntaxException
		 */
		public MailBuilder addErrorAsAttachment(String filename) throws ZipException, IOException, URISyntaxException {
			StringBuilder builder = new StringBuilder();
			for (ExecutionDetail executionDetail : errors) {
				builder.append(executionDetail.getMessage()).append("\n")
						.append(Bytes.getBytes(executionDetail.getThrowable())).append("\n");
			}
			this.attachments.add(Attachments.getInstance(filename, builder.toString().getBytes()));
			return this;
		}

		/**
		 * Enter the configurations in the mail body.<br>
		 * See {@link #addConfig(String, String)} for more details.
		 * 
		 * @return
		 */
		public MailBuilder printConfig(boolean isPrint) {
			this.printConfig = isPrint;
			return this;
		}

		/**
		 * Enter the info in the mail body.<br>
		 * See {@link #addInfo(String)} for more details.
		 * 
		 * @return
		 */
		public MailBuilder printInfo(boolean isPrint) {
			this.printInfo = isPrint;
			return this;
		}

		/**
		 * Enter the root cause of the warnings in the mail body.<br>
		 * See {@link #addWarning(String, Throwable)} for more details.
		 * 
		 * @return
		 */
		public MailBuilder printWarning(boolean isPrint) {
			this.printWarning = isPrint;
			return this;
		}

		/**
		 * Enter the root cause of the errors in the mail body.<br>
		 * See {@link #addError(String, Throwable)} for more details.
		 * 
		 * @return
		 */
		public MailBuilder printError(boolean isPrint) {
			this.printError = isPrint;
			return this;
		}

		/**
		 * Enter the complete stacktrace of the warnings in the mail body.<br>
		 * See {@link #addWarning(String, Throwable)} for more details.
		 * <p>
		 * 
		 * <b>Note: This properties excludes {@link #printWarning()}.</b>
		 * 
		 * @return
		 */
		public MailBuilder printWarningComplete() {
			this.printWarningComplete = true;
			return this;
		}

		/**
		 * Enter the complete stacktrace of the errors in the mail body.<br>
		 * See {@link #addError(String, Throwable)} for more details.
		 * <p>
		 * 
		 * <b>Note: This properties excludes {@link #printError()}.</b>
		 * 
		 * @return
		 */
		public MailBuilder printErrorComplete() {
			this.printErrorComplete = true;
			return this;
		}

		/**
		 * Returns true if configurations are printed in mail.
		 * 
		 * @return
		 */
		public boolean isPrintConfig() {
			return printConfig;
		}

		/**
		 * Returns true if info are printed in mail.
		 * 
		 * @return
		 */
		public boolean isPrintInfo() {
			return printInfo;
		}

		/**
		 * Returns true if the printing of warning causes in mail is enabled.
		 * 
		 * @return
		 */
		public boolean isPrintWarning() {
			return printWarning;
		}

		/**
		 * Returns true if the printing of error causes in mail is enabled.
		 * 
		 * @return
		 */
		public boolean isPrintError() {
			return printError;
		}

		/**
		 * Returns configurations mail (smtp, sender, receiver, ecc...).
		 * 
		 * @return
		 */
		public MailSenderConfiguration getMailConfig() {
			return mailConfig;
		}

		/**
		 * Returns the task that generated the sending of the email. The task is
		 * essential if you want to understand in which part of an execution stream the
		 * email was generated.
		 * 
		 * @return
		 */
		public String getTask() {
			return task;
		}

		/**
		 * Set the task that generated the sending of the email. The task is essential
		 * if you want to understand in which part of an execution stream the email was
		 * generated.
		 * 
		 * @param task
		 * @return
		 */
		public MailBuilder task(String task) {
			this.task = task;
			return this;
		}

		/**
		 * Returns the outcome of the task from which the email was generated.
		 * 
		 * @return
		 */
		public TaskOutcome getTaskOutcome() {
			return taskOutcome;
		}

		/**
		 * The task is finished with success.
		 * 
		 * @return
		 */
		public MailBuilder taskOutcomeSuccess() {
			this.taskOutcome = TaskOutcome.SUCCESS;
			return this;
		}

		/**
		 * The task is finished with success but there were some errors.
		 * 
		 * @return
		 */
		public MailBuilder taskOutcomeSuccessWithErrors() {
			this.taskOutcome = TaskOutcome.SUCCESS_WITH_ERRORS;
			return this;
		}

		/**
		 * The task is failure.
		 * 
		 * @return
		 */
		public MailBuilder taskOutcomeFailure() {
			this.taskOutcome = TaskOutcome.FAILURE;
			return this;
		}

		/**
		 * Quick sending of a message with a specified subject. To be used only in case
		 * of instant e-mail sending, only if you don't need to send an advanced email
		 * with info, errors, attachments, etc.
		 * <p>
		 * This method sends an instant mail overwriting the default message.
		 * <p>
		 * See {@link #send()} for more details about standard mail.
		 * 
		 * @param subject
		 * @param message
		 */
		public void quickSend(String subject, String message) {
			this.subject = subject;
			this.message = message;
			this.overrideMessage = true;
			new MailSender(this).sendMessage();
		}

		/**
		 * Send mail.<br>
		 * This method must be used in case of manual advanced mail configuration.<br>
		 * <br>
		 * See {@link #quickSend(String, String)} to send quick mail.
		 */
		public void send() {
			build();
			new MailSender(this).sendMessage();
		}

		/**
		 * Clean list of info.<br>
		 */
		public MailBuilder clearInfo() {
			info = new HashSet<String>();
			return this;
		}

		/**
		 * Clean list of warnings.<br>
		 */
		public MailBuilder clearWarning() {
			warnings = new HashSet<ExecutionDetail>();
			return this;
		}

		/**
		 * Clean list of errors.<br>
		 */
		public MailBuilder clearError() {
			errors = new HashSet<ExecutionDetail>();
			return this;
		}

		/**
		 * Clean list of info, warnings and errors.<br>
		 */
		public MailBuilder clearAll() {
			clearInfo();
			clearWarning();
			clearError();
			return this;
		}

		public MailBuilder host(String host) {
			this.mailConfig.setHost(host);
			return this;
		}

		public MailBuilder port(String port) {
			this.mailConfig.setPort(port);
			return this;
		}

		public MailBuilder username(String username) {
			this.mailConfig.setUsername(username);
			return this;
		}

		public MailBuilder password(String password) {
			this.mailConfig.setPassword(password);
			return this;
		}

		public MailBuilder sender(String sender) {
			this.mailConfig.setSender(sender);
			return this;
		}

		public MailBuilder receiver(String receiver) {
			this.mailConfig.setReceiver(receiver);
			return this;
		}

		public MailBuilder receiverCC(String receiverCC) {
			this.mailConfig.setReceiverCC(receiverCC);
			return this;
		}

		public MailBuilder receiverBCC(String receiverBCC) {
			this.mailConfig.setReceiverBCC(receiverBCC);
			return this;
		}

		public MailBuilder isAuthenticated(String isAuthenticated) {
			this.mailConfig.setIsAuthenticated(isAuthenticated);
			return this;
		}

		public MailBuilder useTLS(String useTLS) {
			this.mailConfig.setUseTLS(useTLS);
			return this;
		}

		private boolean checkValidity() {
			if (isQuickConfiguration)
				return true;

			if (mailConfig == null) {
				throw new NullPointerException("Missing mail sender configuration");
			} else
				mailConfig.checkValidity();

			if (subject == null || subject.isEmpty()) {
				throw new NullPointerException("Missing subject");
			}

			if (task == null || task.isEmpty()) {
				task = subject;
			}

			if (taskOutcome == null) {
				taskOutcomeSuccess();
			}

			return true;
		}

		/**
		 * Build mail body with your specified configurations (info, config, errors,
		 * warnings, attachments, etc...).
		 * 
		 * @return
		 */
		private MailBuilder build() {
			checkValidity();
			// StringBuilder subjectSb = new StringBuilder();
			// if (this.getWorkflowStep() != null && !reportTO.getWorkflowStep().isEmpty())
			// {
			// subjectSb.append(reportTO.getWorkflowStep()).append(" - ");
			// }
			// subjectSb.append(reportTO.getTaskName()).append(" Report -
			// ").append(reportTO.getOutcome().name());

			if (overrideMessage) {
				return this;
			}

			StringBuffer bodySb = new StringBuffer();

			if (message == null || message.isEmpty())
				bodySb.append("Hi,\n").append("task \"").append(task).append("\" ").append("execution status: ")
						.append(taskOutcome.name()).append(".\n");
			else
				bodySb.append(message).append("\n");

			if (this.getConfigs() != null && !this.getConfigs().isEmpty() && printConfig) {
				bodySb.append("\n").append("=== CONFIG ===").append("\n");
				for (Map.Entry<String, String> entry : this.getConfigs().entrySet()) {
					bodySb.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
				}
			}

			if (this.getInfo() != null && !this.getInfo().isEmpty() && printInfo) {
				bodySb.append("\n").append("=== INFO ===").append("\n");
				for (String info : this.getInfo()) {
					bodySb.append(info).append("\n");
				}
			}

			if (this.getWarnings() != null && !this.getWarnings().isEmpty() && (printWarning || printWarningComplete)) {
				bodySb.append("\n").append("=== WARNINGS ===").append("\n");
				for (ExecutionDetail executionDetail : warnings) {
					bodySb.append(executionDetail.getMessage()).append("\n");
					if (printWarningComplete) {
						bodySb.append(new String(Bytes.getBytes(executionDetail.getThrowable()))).append("\n");
					}
				}
			}

			if (this.getErrors() != null && !this.getErrors().isEmpty() && (printError || printErrorComplete)) {
				bodySb.append("\n").append("=== ERRORS ===").append("\n");
				for (ExecutionDetail executionDetail : errors) {
					bodySb.append(executionDetail.getMessage()).append("\n");
					if (printErrorComplete) {
						bodySb.append(new String(Bytes.getBytes(executionDetail.getThrowable()))).append("\n");
					}
				}
			}

			this.message = bodySb.toString();

			return this;
		}

	}

	/**
	 * Check if a string is an xml.
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isXml(String input) {
		return input.startsWith("<");
	}

	/**
	 * Check if a string is a java.util.Properties.
	 * 
	 * @param input
	 * @return
	 * @throws IOException
	 */
	public static boolean isProps(String input) throws IOException {
		Properties props = new Properties();
		props.load(new StringReader(input));
		return !props.isEmpty();
	}

}
