package com.chebanca.proposta.config.mailsender;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

/**
 * It is used to build the attachments of an email. <br>
 * Attachments are not saved on the disk but are managed by the JVM.
 * 
 * <br>
 * <br>
 * <b>Upload a file attached by a path</b> <br>
 * {@link Attachments}.{@link #getInstance(String)}
 * 
 * <br>
 * <br>
 * <b>Upload and rename a file attached by a path</b> <br>
 * {@link Attachments}.{@link #getInstance(String, String)}
 * 
 * <br>
 * <br>
 * <b>Create an attachment from a byte[]</b> <br>
 * {@link Attachments}.{@link #getInstance(String, byte[])} <br>
 * You can use {@link Bytes} to transform your data structure into byte[].
 * 
 * <br>
 * <br>
 * <b>Create a zip attachment from a list of attachments</b> <br>
 * {@link Attachments}.{@link #getInstance(String, Attachment...)}
 * 
 * <br>
 * <br>
 * <b>Create a cripted zip attachment from a list of attachments</b> <br>
 * {@link Attachments}.{@link #getInstance(String, String, Attachment...)}
 * 
 */
public class Attachments implements Serializable {

	private static final long serialVersionUID = -1073247700591697315L;
	private static final Logger LOGGER = LoggerFactory.getLogger(Attachments.class);

	/**
	 * Upload a file attached by a path. <br>
	 * Path must contains file name.
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static Attachment getInstance(String path) throws IOException {
		LOGGER.debug("Entering getInstance [path: {}]", path);
		File attachment = new File(path);
		return new Attachment().path(path).content(Files.readAllBytes(attachment.toPath())).name(attachment.getName());
	}

	/**
	 * Upload and rename a file attached by a path. <br>
	 * Path must contains file name.<br>
	 * The original attachment is not modified.
	 * 
	 * @param path
	 * @param newName
	 * @return
	 * @throws IOException
	 */
	public static Attachment getInstance(String newName, String path) throws IOException {
		return getInstance(path).name(newName);
	}

	/**
	 * Create an attachment from a byte[].<br>
	 * Attachment will be called <b>filename</b><br>
	 * You can use {@link Bytes} to transform your data structure into a byte[].<br>
	 * 
	 * @param filename
	 * @param content
	 * @return
	 */
	public static Attachment getInstance(String filename, byte[] content) {
		return new Attachment(content, filename);
	}

	/**
	 * Create a zip attachment from a list of attachments.<br>
	 * Zip attachments are not saved on the disk but are managed by the JVM.
	 * 
	 * @param zipFilename
	 * @param attachments
	 * @return
	 * @throws ZipException
	 * @throws IOException
	 */
	public static Attachment getInstance(String zipFilename, Attachment... attachments) throws ZipException, IOException {
		Attachment zipAttachment = createZip(zipFilename, null, attachments);
		zipAttachment.isZip(true);
		return zipAttachment;
	}

	/**
	 * Create a cripted zip attachment from a list of attachments.<br>
	 * Zip attachments are not saved on the disk but are managed by the JVM.
	 * 
	 * @param zipFilename
	 * @param zipPassword
	 * @param attachments
	 * @return
	 * @throws ZipException
	 * @throws IOException
	 */
	public static Attachment getInstance(String zipFilename, String zipPassword, Attachment... attachments) throws ZipException, IOException {
		Attachment zipAttachment = createZip(zipFilename, zipPassword, attachments);
		zipAttachment.isZip(true);
		return zipAttachment;
	}

	/**
	 * Create a zip attachment from a list of attachments. <br>
	 * Password is optional. <br>
	 * If password is not null it will create a zip attachment with password.
	 * 
	 * @param zipName
	 * @param password
	 * @param attachments
	 * @return
	 * @throws ZipException
	 * @throws IOException
	 */
	private static Attachment createZip(String zipName, String password, Attachment... attachments) throws ZipException, IOException {
		return Attachments.getInstance(zipName, getZipContent(password, attachments));
	}

	/**
	 * Create a unique zip attachment from a list of attachments.
	 * 
	 * @param attachments
	 * @param password
	 * @return
	 */
	private static byte[] getZipContent(String password, Attachment... attachments) {
		ZipParameters parameters = getZipParameters(password);
		ByteArrayOutputStream baos = null;
		ZipOutputStream zout = null;
		try {
			baos = new ByteArrayOutputStream();
			zout = new ZipOutputStream(baos);
			for (Attachment zb : attachments) {
				byte file[] = zb.getContent();
				if (file == null || file.length == 0)
					continue;
				String fileName = zb.getName();
				parameters.setFileNameInZip(fileName);
				zout.putNextEntry(null, parameters);
				zout.write(file);
				zout.closeEntry();
			}

			zout.finish();
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ZipException e) {
			e.printStackTrace();
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (zout != null) {
				try {
					zout.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * Create an object of ZipParameters with an optional password.
	 * 
	 * @param password
	 * @return
	 */
	private static ZipParameters getZipParameters(String password) {
		ZipParameters parameters = new ZipParameters();
		parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
		parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
		if (password != null && !password.isEmpty()) {
			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
			parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
			parameters.setPassword(password);
		}
		parameters.setSourceExternalStream(true);
		return parameters;
	}

	/**
	 * <b>Immutable class</b> representing the attachment. <br>
	 * The information on the attachment are the follow:<br>
	 * - <b>path</b>, which is the path of attachment;<br>
	 * - <b>name</b>, which is the name of attachment; <br>
	 * - <b>content</b>, which is the content of attachment.<br>
	 * <br>
	 * {@link #path} excludes {@link #name}. If you specifiy {@link #path} there is
	 * no need to specify {@link #name}.<br>
	 * Specify {@link #path} only to upload a file saved on the disk. <br>
	 * Specify {@link #name} only to load content of object managed in JVM.
	 */
	public static class Attachment implements Serializable {
		private static final long serialVersionUID = -4775419089660556376L;

		/**
		 * Represent path of file to upload. <br>
		 * Path must contain file name.
		 */
		private String path;

		/**
		 * Represent attachment name. <br>
		 * Name must contain extension.
		 */
		private String name;

		/**
		 * Represent attachment content.
		 */
		protected byte[] content;

		/**
		 * Is this a zip attachment?
		 */
		private boolean isZip = false;

		/**
		 * List of zip attachments.
		 */
		private List<Attachment> zipAttachments = new ArrayList<Attachment>();

		/**
		 * Creates a new attachment from an indicated path
		 * @param path
		 */
		public Attachment(String path) {
			this.path = path;
		}

		/**
		 * Creates a new attachment with a path and a name
		 * <br>
		 * The name must contain a valid extension
		 * <br>
		 * @param path
		 * @param name
		 */
		public Attachment(String path, String name) {
			this.path = path;
			this.name = name;
		}

		/**
		 * Creates a new byte stream attachment
		 * <br>
		 * The name must contain a valid extension
		 * <br> 
		 * @param content The byte stream you want to send into the email
		 * @param name A valid file name, including the extension
		 */
		public Attachment(byte[] content, String name) {
			this.name = name;
			this.content = content;
		}

		public Attachment() {
		}

		/**
		 * Return attachment path.
		 * 
		 * @return
		 */
		public String getPath() {
			return path;
		}

		/**
		 * Set attachment path.
		 * 
		 * @param path
		 * @return
		 */
		private Attachment path(String path) {
			this.path = path;
			return this;
		}

		/**
		 * Return attachment name.
		 * 
		 * @return
		 */
		public String getName() {
			return name;
		}

		/**
		 * Set attachment name.
		 * 
		 * @param name
		 * @return
		 */
		private Attachment name(String name) {
			this.name = name;
			return this;
		}

		/**
		 * Return attachment content.
		 * 
		 * @return
		 */
		public byte[] getContent() {
			return content;
		}

		/**
		 * Set attachment content.
		 * 
		 * @param content
		 * @return
		 */
		private Attachment content(byte[] content) {
			this.content = content;
			return this;
		}

		/**
		 * Is this a zip attachment?
		 * 
		 * @return
		 */
		public boolean isZip() {
			return isZip;
		}

		/**
		 * Specify if this is a zip attachment.
		 * 
		 * @param isZip
		 * @return
		 */
		public Attachment isZip(boolean isZip) {
			this.isZip = isZip;
			return this;
		}

		/**
		 * Return list of zip attachments.<br>
		 * The method only works in case of zip-type attachment returns an
		 * {@link IllegalStateException}.<br>
		 * See {@link #isZip()}.<br
		 * 
		 * @return
		 */
		public List<Attachment> getZipAttachments() {
			if (isZip)
				return zipAttachments;
			throw new IllegalStateException("Current attachment is not a zip, it hasn't a list of attachments.");
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Attachment [path=");
			builder.append(path);
			builder.append(", name=");
			builder.append(name);
			builder.append(", content=");
			builder.append(Arrays.toString(content));
			builder.append("]");
			return builder.toString();
		}
	}

}
