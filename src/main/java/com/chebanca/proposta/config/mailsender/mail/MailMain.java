package com.chebanca.proposta.config.mailsender.mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.lingala.zip4j.exception.ZipException;

/**
 * Main class. Se sono state configurate le variabili di sistema verrà avviata
 * la modalità automatica, altrimenti partirà la modalità da prompt dei comandi.
 */
public class MailMain {

	protected static final String TITLE = "___  ___      _ _ _____                _           \r\n" + "|  \\/  |     (_) /  ___|              | |          \r\n" + "| .  . | __ _ _| \\ `--.  ___ _ __   __| | ___ _ __ \r\n" + "| |\\/| |/ _` | | |`--. \\/ _ \\ '_ \\ / _` |/ _ \\ '__|\r\n" + "| |  | | (_| | | /\\__/ /  __/ | | | (_| |  __/ |   \r\n" + "\\_|  |_/\\__,_|_|_\\____/ \\___|_| |_|\\__,_|\\___|_|   \r\n" + "                                                   \r\n" + "                                                   ";
	protected static final String HELP = "-h\thelp\n" + "-s\tadd subject\n" + "-m\tadd message\n" + "-t\tadd task name\n" + "-o\tadd task outcome\n" + "-i\tadd info\n" + "-c\tadd config (key, value)\n" + "-a\tadd attachment path\nquit\tquick program";
	protected static final String PROMPT = "> ";
	protected static BufferedReader LINE_READER = new BufferedReader(new InputStreamReader(System.in));
	private static Logger logger = LoggerFactory.getLogger(MailMain.class);

	// Comandi
	protected static final String CMD_HELP = "-h";
	protected static final String CMD_SUBJECT = "-s";
	protected static final String CMD_MESSAGE = "-m";
	protected static final String CMD_TASK = "-t";
	protected static final String CMD_TASK_OUTCOME = "-o";
	protected static final String CMD_INFO = "-i";
	protected static final String CMD_CONFIG = "-c";
	protected static final String CMD_ATTACHMENT = "-a";
	protected static final String CMD_SEND = "-send";

	public static void main(String[] args) {
		System.out.println(MailMain.TITLE);
		try {
			if (hasSystemProperties()) {
				// -Dmc=C:\\Develop\\Gatelab\\Workspace\\Trunk\\util\\com.gatelab.resource.mailsender\\env\\local\\envConf\\mail_configuration_my.xml
				// -Ds=my-subject -Dm=my-message -Dt=task -Do=s -Di=my-info
				new MailBySystemProperties();
			} else {
				new MailByPrompt();
			}
		} catch (IOException | ZipException | URISyntaxException e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * Ci sono variabili di sistema?
	 * 
	 * @return
	 */
	private static boolean hasSystemProperties() {
		return !MailMain.isNullOrEmpty(System.getProperty("s")) || !MailMain.isNullOrEmpty(System.getProperty("m")) || !MailMain.isNullOrEmpty(System.getProperty("t")) || !MailMain.isNullOrEmpty(System.getProperty("o")) || !MailMain.isNullOrEmpty(System.getProperty("i")) || !MailMain.isNullOrEmpty(System.getProperty("c")) || !MailMain.isNullOrEmpty(System.getProperty("a"));
	}

	/**
	 * Verifica se la stringa di input è null o vuota.
	 * 
	 * @param s
	 * @return
	 */
	protected static boolean isNullOrEmpty(String s) {
		return s == null || s.isEmpty();
	}

	/**
	 * Legge una linea da prompt dei comandi.
	 * 
	 * @return
	 * @throws IOException
	 */
	protected static String readLine() throws IOException {
		return LINE_READER.readLine();
	}
}

