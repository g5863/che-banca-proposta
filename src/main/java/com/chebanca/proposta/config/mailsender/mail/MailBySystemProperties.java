package com.chebanca.proposta.config.mailsender.mail;

import java.io.IOException;

import com.chebanca.proposta.config.mailsender.MailFactory;
import com.chebanca.proposta.config.mailsender.MailFactory.MailBuilder;

/**
 * Permette di inviare una mail configurando i parametri come variabili di
 * sistema.
 */
public class MailBySystemProperties {

	public MailBySystemProperties() throws IOException {
		mailBySystemProperties();
	}

	/**
	 * -mc=C:\\Develop\\Gatelab\\Workspace\\Trunk\\util\\com.gatelab.resource.mailsender\\env\\local\\envConf\\mail_configuration_my.xml
	 * -s=my-subject -m=my-message -t=task -to=s -i=my-info
	 * 
	 * @param args
	 * @throws IOException
	 */
	private void mailBySystemProperties() throws IOException {
		MailBuilder mb = MailFactory.createMail(System.getProperty("mc"));
		mb.subject(System.getProperty("s"));
		mb.message(System.getProperty("m"));
		mb.task(System.getProperty("t"));

		String taskOutcome = System.getProperty("o");
		if (taskOutcome.equals("s")) {
			mb.taskOutcomeSuccess();
		} else if (taskOutcome.equals("se")) {
			mb.taskOutcomeSuccessWithErrors();
		} else if (taskOutcome.equals("f")) {
			mb.taskOutcomeFailure();
		} else {
			System.err.println("Task not recognized");
			return;
		}

		mb.send();
	}

}
