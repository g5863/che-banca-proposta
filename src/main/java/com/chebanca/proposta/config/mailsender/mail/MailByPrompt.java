package com.chebanca.proposta.config.mailsender.mail;

import java.io.IOException;
import java.net.URISyntaxException;

import com.chebanca.proposta.config.mailsender.Attachments;
import com.chebanca.proposta.config.mailsender.MailConfig;
import com.chebanca.proposta.config.mailsender.MailFactory;
import com.chebanca.proposta.config.mailsender.MailFactory.MailBuilder;

import net.lingala.zip4j.exception.ZipException;

/**
 * Permette di inviare una mail digitando i parametri da prompt.
 */
public class MailByPrompt {

	public MailByPrompt() throws IOException, ZipException, URISyntaxException {
		mailByCommandLine();
	}

	private void mailByCommandLine() throws IOException, ZipException, URISyntaxException {
		// C:\\Develop\\Gatelab\\Workspace\\Trunk\\util\\com.gatelab.resource.mailsender\\env\\local\\envConf\\mail_configuration_my.xml

		// Mail configuration type
		MailBuilder mb = null;
		{
			System.out.println(MailMain.PROMPT + "Insert your mail configuration type (x=xml, p=props, m=manual): ");
			String mailConfigType = MailMain.readLine();
			while (!mailConfigType.equals("x") && !mailConfigType.equals("p") && !mailConfigType.equals("m")) {
				System.err.println("Incorrect mail configuration type (x=xml, p=props, m=manual): ");
				mailConfigType = MailMain.readLine();
			}
			if (mailConfigType.equals("x")) {
				System.out.println(MailMain.PROMPT + "Insert your xml mail configuration file path: ");
				mb = MailFactory.createMail(MailMain.readLine());
			} else if (mailConfigType.equals("p")) {
				System.out.println(MailMain.PROMPT + "Insert your properties mail configuration file path: ");
				mb = MailFactory.createMail(MailMain.readLine());
			} else { // if (mailConfigType.equals("m")) {
				System.out.println(MailMain.PROMPT + "Insert host: ");
				String host = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert port: ");
				String port = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert username: ");
				String username = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert password: ");
				String password = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert sender: ");
				String sender = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert receiver: ");
				String receiver = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert isAuthenticated: ");
				String isAuthenticated = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert useTLS: ");
				String useTLS = MailMain.readLine();
				mb = MailFactory.createMail(MailConfig.getBuilder().host(host).port(port).username(username).password(password).sender(sender).receiver(receiver).isAuthenticated(isAuthenticated).useTLS(useTLS).build());
			}
		}

		System.out.println(MailMain.PROMPT);
		String command = MailMain.readLine();
		while (!command.equals("quit")) {

			if (command.equals(MailMain.CMD_TASK)) {
				System.out.println(MailMain.PROMPT + "Insert your task: ");
				mb.task(MailMain.readLine());
			}

			else if (command.equals(MailMain.CMD_TASK_OUTCOME)) {
				System.out.println(MailMain.PROMPT + "Insert your task (s=success, se=success with errors, f=failure): ");
				String taskOutcome = MailMain.readLine();
				if (taskOutcome.equals("s")) {
					mb.taskOutcomeSuccess();
				} else if (taskOutcome.equals("se")) {
					mb.taskOutcomeSuccessWithErrors();
				} else if (taskOutcome.equals("f")) {
					mb.taskOutcomeFailure();
				} else {
					System.err.println("Task not recognized");
				}
			}

			else if (command.equals(MailMain.CMD_SUBJECT)) {
				System.out.println(MailMain.PROMPT + "Insert your mail subject: ");
				mb.subject(MailMain.readLine());
			}

			else if (command.equals(MailMain.CMD_MESSAGE)) {
				System.out.println(MailMain.PROMPT + "Insert your message: ");
				mb.message(MailMain.readLine());
			}

			else if (command.equals(MailMain.CMD_INFO)) {
				System.out.println(MailMain.PROMPT + "Insert your info: ");
				mb.addInfo(MailMain.readLine());
			}

			else if (command.equals(MailMain.CMD_CONFIG)) {
				System.out.println(MailMain.PROMPT + "Insert your config key: ");
				String configKey = MailMain.readLine();
				System.out.println(MailMain.PROMPT + "Insert your config value: ");
				String configValue = MailMain.readLine();
				mb.addConfig(configKey, configValue);
			}

			else if (command.equals(MailMain.CMD_ATTACHMENT)) {
				System.out.println(MailMain.PROMPT + "Insert your attachment path: ");
				mb.addAttachments(Attachments.getInstance(MailMain.readLine()));
			}

			else if (command.equals(MailMain.CMD_SEND)) {
				mb.send();
			}

			else if (command.equals(MailMain.CMD_HELP)) {
				System.out.println(MailMain.PROMPT + MailMain.HELP);
			}

			else {
				System.err.println("Command not recognized");
			}

			System.out.println("\n> ");
			command = MailMain.readLine();
		}
	}
}
