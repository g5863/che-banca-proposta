package com.chebanca.proposta.config.mailsender;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chebanca.proposta.config.mailsender.Attachments.Attachment;
import com.chebanca.proposta.config.mailsender.MailFactory.MailBuilder;


/**
 * Send the configured email.
 */
public class MailSender {

	private Logger logger = LoggerFactory.getLogger(MailSender.class);

	protected Session session;
	private MailBuilder mailBuilder;

	public MailSender(MailBuilder mailBuilder) {
		this.mailBuilder = mailBuilder;
		createSession();
	}

	public void createSession() {
		logger.debug("creating session");
		
		if (mailBuilder == null) {
			String msgErr = "MailBuilder is null";
			NullPointerException npe = new NullPointerException(msgErr);
			logger.error(msgErr, npe);
			throw npe;
		}
		
		if (mailBuilder.getMailConfig() == null) {
			String msgErr = "MailConfig is null";
			NullPointerException npe = new NullPointerException(msgErr);
			logger.error(msgErr, npe);
			throw npe;
		}
		
		if (!mailBuilder.getMailConfig().checkValidity()) {
			String msgErr = "MailConfig is incorrect. " + mailBuilder.getMailConfig().toString();
			NullPointerException npe = new NullPointerException(msgErr);
			logger.error(msgErr, npe);
			throw npe;
		}

		Properties props = new Properties();
		props.put("mail.smtp.host", mailBuilder.getMailConfig().getHost());
		props.put("mail.smtp.port", mailBuilder.getMailConfig().getPort());
		props.put("mail.smtp.auth", mailBuilder.getMailConfig().getIsAuthenticated());
		props.put("mail.smtp.starttls.enable", mailBuilder.getMailConfig().getUseTLS());
		props.put("mail.smtp.auth.mechanisms", "LOGIN");
		
		/**
		 * CONFIGURATION GOOGLE SERVER SMTP<br>
		 * props.put("mail.smtp.host", "smtp.gmail.com"); <br>
		 * props.put("mail.smtp.port", "587"); <br>
		 * props.put("mail.smtp.auth", "true"); <br>
		 * props.put("mail.smtp.starttls.enable", "true"); // TLS
		 */

		session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailBuilder.getMailConfig().getUsername(), mailBuilder.getMailConfig().getPassword());
			}
		});

		session.setDebug(true);
	}

	public void checkSession() {
		if (session == null) {
			createSession();
		}
	}

	public void sendMessage() {
		Message message = buildMessage();
		checkSession();
		try {
			logger.info("Mail configurations " + getMailConfigurations());
			Transport.send(message);
			logger.info("Mail sent");
		} catch (Exception e) {
			logger.error("cannot send mail message ", e);
		}
	}
	
	/**
	 * Returns mail configurations in string format.
	 * @return
	 */
	public String getMailConfigurations() {
		StringBuilder sbConfigurations = new StringBuilder("[");
		sbConfigurations.append("Host: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getHost());
		sbConfigurations.append(", Port: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getPort());
		sbConfigurations.append(", Username: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getUsername());
		sbConfigurations.append(", Password: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getPassword());
		sbConfigurations.append(", Sender: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getSender());
		sbConfigurations.append(", Receivers: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getReceiver());
		sbConfigurations.append(", ReceiversCC: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getReceiverCC());
		sbConfigurations.append(", ReceiversBCC: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getReceiverBCC());
		sbConfigurations.append(", isAuthenticated: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getIsAuthenticated());
		sbConfigurations.append(", useTLS: ");
		sbConfigurations.append(mailBuilder.getMailConfig().getUseTLS());
		sbConfigurations.append("]");
		return sbConfigurations.toString();
	}

	private Message buildMessage() {
		Message message = null;
		try {
			message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailBuilder.getMailConfig().getSender()));
			
			// TO
			String recipientList = mailBuilder.getMailConfig().getReceiver();
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientList));

			// CC
			String recipientCCList = mailBuilder.getMailConfig().getReceiverCC();
			if (recipientCCList != null && !recipientCCList.isEmpty())
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(recipientCCList));
			
			// BCC
			String recipientBCCList = mailBuilder.getMailConfig().getReceiverBCC();
			if (recipientBCCList != null && !recipientBCCList.isEmpty())
				message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(recipientBCCList));

			message.setSubject(mailBuilder.getSubject());

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			if (mailBuilder.isHtmlMessage()) {
				messageBodyPart.setContent(mailBuilder.getMessage(), "text/html");
			} else {
				messageBodyPart.setText(mailBuilder.getMessage());
			}

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Manage attachments
			if (mailBuilder.getAttachments().size() > 0) {
				for (Attachment attachment : mailBuilder.getAttachments()) {
					messageBodyPart = new MimeBodyPart();
					DataSource source = new ByteArrayDataSource(attachment.getContent(), "application/octet-stream");
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(attachment.getName());
					multipart.addBodyPart(messageBodyPart);
				}
			}

			// Send the complete message parts
			message.setContent(multipart);

		} catch (MessagingException e) {
			logger.error("Exception on building message: ", e);
			throw new RuntimeException(e);
		}
		return message;
	}

}
