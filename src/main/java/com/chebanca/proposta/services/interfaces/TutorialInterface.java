package com.chebanca.proposta.services.interfaces;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.chebanca.proposta.models.Tutorial;

@Component
public interface TutorialInterface {
	public List<Tutorial> searchAllTutorials(String title);
	public Optional<Tutorial> searchById(long id);
	public Tutorial createTutorial(Tutorial tutorial);
	public Tutorial updateTutorial(long id, Tutorial tutorial);
	public HttpStatus deleteTutorial(long id);
	public HttpStatus deleteAllTutorials();
	public List<Tutorial> findByPublished();
}
