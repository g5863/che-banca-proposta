package com.chebanca.proposta.services.interfaces;

import java.util.Set;

import com.chebanca.proposta.models.Portafoglio;
import com.chebanca.proposta.models.Utente;

public interface PortafoglioInterface {

	Set<Portafoglio> getPortafoglio(Utente utente);
}
