package com.chebanca.proposta.services.interfaces;

import org.springframework.stereotype.Component;

import com.chebanca.proposta.models.Utente;

@Component
public interface UtenteInterface {

	public Utente createUtente(Utente utente);
}
