package com.chebanca.proposta.services.interfaces;

import java.util.List;

import com.chebanca.proposta.models.Proposta;
import com.chebanca.proposta.models.Strumento;
import com.chebanca.proposta.models.Utente;

public interface PropostaInterface {

	List<Proposta> searchAllProposes();
	List<Strumento> getStrumentsByUser(Utente utente);

}
