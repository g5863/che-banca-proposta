package com.chebanca.proposta.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chebanca.proposta.models.Utente;
import com.chebanca.proposta.repository.UtenteRepository;
import com.chebanca.proposta.services.interfaces.UtenteInterface;

@Service
public class UtenteService implements UtenteInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(UtenteService.class);
	
	@Autowired
	private UtenteRepository utenteRepository;
	
	@Override
	public Utente createUtente(Utente utente) {
		try {

			LOGGER.info("");
			return utenteRepository.save(utente);
		} catch (Exception e) {
			return null;
		}
	}

}
