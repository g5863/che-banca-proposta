package com.chebanca.proposta.services;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chebanca.proposta.models.Portafoglio;
import com.chebanca.proposta.models.Utente;
import com.chebanca.proposta.repository.PortafoglioRepository;
import com.chebanca.proposta.services.interfaces.PortafoglioInterface;

@Service
public class PortafoglioService implements PortafoglioInterface{

	private static final Logger LOGGER = LoggerFactory.getLogger(PortafoglioService.class);
	
	@Autowired
	private PortafoglioRepository portafoglioRepo;
	
	@Override
	public Set<Portafoglio> getPortafoglio(Utente utente) {
		Set<Portafoglio> portafoglio = new HashSet<>();
		try {
			portafoglio = portafoglioRepo.findByUtente(utente);
			
		}catch(Exception e) {
			LOGGER.error("Errore {}", e.getMessage());
		}
		return portafoglio;
	}
}
