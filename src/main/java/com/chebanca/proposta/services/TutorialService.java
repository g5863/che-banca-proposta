package com.chebanca.proposta.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.chebanca.proposta.models.Tutorial;
import com.chebanca.proposta.repository.TutorialRepository;
import com.chebanca.proposta.services.interfaces.TutorialInterface;

@Service
public class TutorialService implements TutorialInterface {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TutorialService.class);
	
	@Autowired
	TutorialRepository tutorialRepository;

	@Override
	public List<Tutorial> searchAllTutorials(String title) {
		try {
			LOGGER.info("CIAO");
			List<Tutorial> tutorials = new ArrayList<>();
			if (title == null)
				tutorialRepository.findAll().forEach(tutorials::add);
			else
				tutorialRepository.findByTitleContaining(title).forEach(tutorials::add);

			return tutorials;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Optional<Tutorial> searchById(long id) {
		return tutorialRepository.findById(id);
	}

	@Override
	public Tutorial createTutorial(Tutorial tutorial) {
		try {
			return tutorialRepository.save(new Tutorial(tutorial.getTitle(), tutorial.getDescription(), false));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Tutorial updateTutorial(long id, Tutorial tutorialRequest) {
		Optional<Tutorial> tutorialData = tutorialRepository.findById(id);
		if (tutorialData.isPresent()) {
			Tutorial upd = tutorialData.get();
			upd.setTitle(tutorialRequest.getTitle());
			upd.setDescription(tutorialRequest.getDescription());
			upd.setPublished(tutorialRequest.isPublished());
			return tutorialRepository.save(upd);
		} else
			return null;
	}

	@Override
	public HttpStatus deleteTutorial(long id) {
		try {
			tutorialRepository.deleteById(id);
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	@Override
	public HttpStatus deleteAllTutorials() {
		try {
			tutorialRepository.deleteAll();
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	@Override
	public List<Tutorial> findByPublished() {
		try {
			return tutorialRepository.findByPublished(true);
		} catch (Exception e) {
			return null;
		}
	}

}
